﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HalconDotNet;
using MvCamCtrl.NET;
using VisionDetectionWifi.halconAlgorithm;

namespace VisionDetectionWifi
{
    public partial class Wifi_Detection_Form : Form
    {
        private MyCamera.MV_CC_DEVICE_INFO_LIST m_pDeviceList;
        private MyCamera m_pMyCamera;
        private bool m_bGrabbing;

        // ch:用于从驱动获取图像的缓存 | en:Buffer for getting image from driver
        private UInt32 m_nBufSizeForDriver = 3072 * 2048 * 3;

        private byte[] m_pBufForDriver = new byte[3072 * 2048 * 3];

        // ch:用于保存图像的缓存 | en:Buffer for saving image
        private UInt32 m_nBufSizeForSaveImage = 3072 * 2048 * 3 * 3 + 2048;

        private byte[] m_pBufForSaveImage = new byte[3072 * 2048 * 3 * 3 + 2048];

        public Wifi_Detection_Form()
        {
            InitializeComponent();
            m_pDeviceList = new MyCamera.MV_CC_DEVICE_INFO_LIST();
            m_bGrabbing = false;
            DeviceListAcq();
        }

        #region 1.查找设备

        private void BnEnum_Click(object sender, EventArgs e)
        {
            DeviceListAcq();
        }

        private void DeviceListAcq()
        {
            int nRet;
            // ch:创建设备列表 en:Create Device List
            System.GC.Collect();
            cbDeviceList.Items.Clear();
            nRet = MyCamera.MV_CC_EnumDevices_NET(MyCamera.MV_GIGE_DEVICE | MyCamera.MV_USB_DEVICE, ref m_pDeviceList);
            if (0 != nRet)
            {
                ShowErrorMsg("Enumerate devices fail!", 0);
                return;
            }

            // ch:在窗体列表中显示设备名 | en:Display device name in the form list
            for (int i = 0; i < m_pDeviceList.nDeviceNum; i++)
            {
                MyCamera.MV_CC_DEVICE_INFO device = (MyCamera.MV_CC_DEVICE_INFO)Marshal.PtrToStructure(m_pDeviceList.pDeviceInfo[i], typeof(MyCamera.MV_CC_DEVICE_INFO));
                if (device.nTLayerType == MyCamera.MV_GIGE_DEVICE)
                {
                    IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(device.SpecialInfo.stGigEInfo, 0);
                    MyCamera.MV_GIGE_DEVICE_INFO gigeInfo = (MyCamera.MV_GIGE_DEVICE_INFO)Marshal.PtrToStructure(buffer, typeof(MyCamera.MV_GIGE_DEVICE_INFO));
                    if (gigeInfo.chUserDefinedName != "")
                    {
                        cbDeviceList.Items.Add("GigE: " + gigeInfo.chUserDefinedName + " (" + gigeInfo.chSerialNumber + ")");
                    }
                    else
                    {
                        cbDeviceList.Items.Add("GigE: " + gigeInfo.chManufacturerName + " " + gigeInfo.chModelName + " (" + gigeInfo.chSerialNumber + ")");
                    }
                }
                else if (device.nTLayerType == MyCamera.MV_USB_DEVICE)
                {
                    IntPtr buffer = Marshal.UnsafeAddrOfPinnedArrayElement(device.SpecialInfo.stUsb3VInfo, 0);
                    MyCamera.MV_USB3_DEVICE_INFO usbInfo = (MyCamera.MV_USB3_DEVICE_INFO)Marshal.PtrToStructure(buffer, typeof(MyCamera.MV_USB3_DEVICE_INFO));
                    if (usbInfo.chUserDefinedName != "")
                    {
                        cbDeviceList.Items.Add("USB: " + usbInfo.chUserDefinedName + " (" + usbInfo.chSerialNumber + ")");
                    }
                    else
                    {
                        cbDeviceList.Items.Add("USB: " + usbInfo.chManufacturerName + " " + usbInfo.chModelName + " (" + usbInfo.chSerialNumber + ")");
                    }
                }
            }

            // ch:选择第一项 | en:Select the first item
            if (m_pDeviceList.nDeviceNum != 0)
            {
                cbDeviceList.SelectedIndex = 0;
            }
        }

        #endregion 1.查找设备

        #region 2.错误信息显示

        // ch:显示错误信息 | en:Show error message
        private void ShowErrorMsg(string csMessage, int nErrorNum)
        {
            string errorMsg;
            if (nErrorNum == 0)
            {
                errorMsg = csMessage;
            }
            else
            {
                errorMsg = csMessage + ": Error =" + String.Format("{0:X}", nErrorNum);
            }
            switch (nErrorNum)
            {
                case MyCamera.MV_E_HANDLE: errorMsg += " Error or invalid handle "; break;
                case MyCamera.MV_E_SUPPORT: errorMsg += " Not supported function "; break;
                case MyCamera.MV_E_BUFOVER: errorMsg += " Cache is full "; break;
                case MyCamera.MV_E_CALLORDER: errorMsg += " Function calling order error "; break;
                case MyCamera.MV_E_PARAMETER: errorMsg += " Incorrect parameter "; break;
                case MyCamera.MV_E_RESOURCE: errorMsg += " Applying resource failed "; break;
                case MyCamera.MV_E_NODATA: errorMsg += " No data "; break;
                case MyCamera.MV_E_PRECONDITION: errorMsg += " Precondition error, or running environment changed "; break;
                case MyCamera.MV_E_VERSION: errorMsg += " Version mismatches "; break;
                case MyCamera.MV_E_NOENOUGH_BUF: errorMsg += " Insufficient memory "; break;
                case MyCamera.MV_E_UNKNOW: errorMsg += " Unknown error "; break;
                case MyCamera.MV_E_GC_GENERIC: errorMsg += " General error "; break;
                case MyCamera.MV_E_GC_ACCESS: errorMsg += " Node accessing condition error "; break;
                case MyCamera.MV_E_ACCESS_DENIED: errorMsg += " No permission "; break;
                case MyCamera.MV_E_BUSY: errorMsg += " Device is busy, or network disconnected "; break;
                case MyCamera.MV_E_NETER: errorMsg += " Network error "; break;
            }
            MessageBox.Show(errorMsg, "PROMPT");
        }

        #endregion 2.错误信息显示

        #region 3.打开设备

        private void bnOpen_Click(object sender, EventArgs e)
        {
            if (m_pDeviceList.nDeviceNum == 0 || cbDeviceList.SelectedIndex == -1)
            {
                ShowErrorMsg("请选择设备！", 0);
                return;
            }
            int nRet = -1;

            // ch:获取选择的设备信息 | en:Get selected device information
            MyCamera.MV_CC_DEVICE_INFO device =
                (MyCamera.MV_CC_DEVICE_INFO)Marshal.PtrToStructure(m_pDeviceList.pDeviceInfo[cbDeviceList.SelectedIndex],
                                                              typeof(MyCamera.MV_CC_DEVICE_INFO));

            // ch:打开设备 | en:Open device
            if (null == m_pMyCamera)
            {
                m_pMyCamera = new MyCamera();
                if (null == m_pMyCamera)
                {
                    return;
                }
            }

            nRet = m_pMyCamera.MV_CC_CreateDevice_NET(ref device);
            if (MyCamera.MV_OK != nRet)
            {
                return;
            }

            nRet = m_pMyCamera.MV_CC_OpenDevice_NET();
            if (MyCamera.MV_OK != nRet)
            {
                m_pMyCamera.MV_CC_DestroyDevice_NET();
                ShowErrorMsg("Device open fail!", nRet);
                return;
            }

            // ch:探测网络最佳包大小(只对GigE相机有效) | en:Detection network optimal package size(It only works for the GigE camera)
            if (device.nTLayerType == MyCamera.MV_GIGE_DEVICE)
            {
                int nPacketSize = m_pMyCamera.MV_CC_GetOptimalPacketSize_NET();
                if (nPacketSize > 0)
                {
                    nRet = m_pMyCamera.MV_CC_SetIntValue_NET("GevSCPSPacketSize", (uint)nPacketSize);
                    if (nRet != MyCamera.MV_OK)
                    {
                        Console.WriteLine("Warning: Set Packet Size failed {0:x8}", nRet);
                    }
                }
                else
                {
                    Console.WriteLine("Warning: Get Packet Size failed {0:x8}", nPacketSize);
                }
            }

            // ch:设置采集连续模式 | en:Set Continues Aquisition Mode
            m_pMyCamera.MV_CC_SetEnumValue_NET("AcquisitionMode", 2);// ch:工作在连续模式 | en:Acquisition On Continuous Mode
            m_pMyCamera.MV_CC_SetEnumValue_NET("TriggerMode", 0);    // ch:连续模式 | en:Continuous

            bnGetParam_Click(null, null);// ch:获取参数 | en:Get parameters

            // ch:控件操作 | en:Control operation
            SetCtrlWhenOpen();
        }

        private void SetCtrlWhenOpen()
        {
            bnOpen.Enabled = false;

            bnClose.Enabled = true;
            bnStartGrab.Enabled = true;
            bnStopGrab.Enabled = false;
            bnContinuesMode.Enabled = true;
            bnContinuesMode.Checked = true;
            bnTriggerMode.Enabled = true;
            cbSoftTrigger.Enabled = false;
            bnTriggerExec.Enabled = false;

            tbExposure.Enabled = true;
            tbGain.Enabled = true;
            tbFrameRate.Enabled = true;
            bnGetParam.Enabled = true;
            bnSetParam.Enabled = true;
        }

        #endregion 3.打开设备

        #region 4.关闭设备

        private void bnClose_Click(object sender, EventArgs e)
        {
            // ch:关闭设备 | en:Close Device
            int nRet;

            nRet = m_pMyCamera.MV_CC_CloseDevice_NET();
            if (MyCamera.MV_OK != nRet)
            {
                return;
            }

            nRet = m_pMyCamera.MV_CC_DestroyDevice_NET();
            if (MyCamera.MV_OK != nRet)
            {
                return;
            }

            // ch:控件操作 | en:Control Operation
            SetCtrlWhenClose();

            // ch:取流标志位清零 | en:Reset flow flag bit
            m_bGrabbing = false;
        }

        private void SetCtrlWhenClose()
        {
            bnOpen.Enabled = true;

            bnClose.Enabled = false;
            bnStartGrab.Enabled = false;
            bnStopGrab.Enabled = false;
            bnContinuesMode.Enabled = false;
            bnTriggerMode.Enabled = false;
            cbSoftTrigger.Enabled = false;
            bnTriggerExec.Enabled = false;

            bnSaveBmp.Enabled = false;
            bnSaveJpg.Enabled = false;
            tbExposure.Enabled = false;
            tbGain.Enabled = false;
            tbFrameRate.Enabled = false;
            bnGetParam.Enabled = false;
            bnSetParam.Enabled = false;
        }

        #endregion 4.关闭设备

        #region 5.连续采集

        private void bnContinuesMode_CheckedChanged(object sender, EventArgs e)
        {
            if (bnContinuesMode.Checked)
            {
                m_pMyCamera.MV_CC_SetEnumValue_NET("TriggerMode", 0);
                cbSoftTrigger.Enabled = false;
                bnTriggerExec.Enabled = false;
            }
        }

        #endregion 5.连续采集

        #region 6.触发模式

        private void bnTriggerMode_CheckedChanged(object sender, EventArgs e)
        {
        }

        #endregion 6.触发模式

        #region 7.开始采集

        private void bnStartGrab_Click(object sender, EventArgs e)
        {
        }

        private void SetCtrlWhenStartGrab()
        {
            bnStartGrab.Enabled = false;
            bnStopGrab.Enabled = true;

            if (bnTriggerMode.Checked && cbSoftTrigger.Checked)
            {
                bnTriggerExec.Enabled = true;
            }

            bnSaveBmp.Enabled = true;
            bnSaveJpg.Enabled = true;
        }

        #endregion 7.开始采集

        #region 8.停止采集

        private void bnStopGrab_Click(object sender, EventArgs e)
        {
        }

        private void SetCtrlWhenStopGrab()
        {
            bnStartGrab.Enabled = true;
            bnStopGrab.Enabled = false;

            bnTriggerExec.Enabled = false;

            bnSaveBmp.Enabled = false;
            bnSaveJpg.Enabled = false;
        }

        #endregion 8.停止采集

        #region 9.软触发

        private void cbSoftTrigger_CheckedChanged(object sender, EventArgs e)
        {
        }

        #endregion 9.软触发

        #region 10.软停止一次

        private void bnTriggerExec_Click(object sender, EventArgs e)
        {
        }

        #endregion 10.软停止一次

        #region 11.保存图片-Bmp

        private void bnSaveBmp_Click(object sender, EventArgs e)
        {
            //雷涛
            int nRet;
            UInt32 nPayloadSize = 0;
            MyCamera.MVCC_INTVALUE stParam = new MyCamera.MVCC_INTVALUE();
            nRet = m_pMyCamera.MV_CC_GetIntValue_NET("PayloadSize", ref stParam);
            if (MyCamera.MV_OK != nRet)
            {
                ShowErrorMsg("Get PayloadSize failed", nRet);
                return;
            }
            nPayloadSize = stParam.nCurValue;
            if (nPayloadSize > m_nBufSizeForDriver)
            {
                m_nBufSizeForDriver = nPayloadSize;
                m_pBufForDriver = new byte[m_nBufSizeForDriver];

                // ch:同时对保存图像的缓存做大小判断处理 | en:Determine the buffer size to save image
                // ch:BMP图片大小：width * height * 3 + 2048(预留BMP头大小) | en:BMP image size: width * height * 3 + 2048 (Reserved for BMP header)
                m_nBufSizeForSaveImage = m_nBufSizeForDriver * 3 + 2048;
                m_pBufForSaveImage = new byte[m_nBufSizeForSaveImage];
            }

            IntPtr pData = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForDriver, 0);
            MyCamera.MV_FRAME_OUT_INFO_EX stFrameInfo = new MyCamera.MV_FRAME_OUT_INFO_EX();
            // ch:超时获取一帧，超时时间为1秒 | en:Get one frame timeout, timeout is 1 sec
            nRet = m_pMyCamera.MV_CC_GetOneFrameTimeout_NET(pData, m_nBufSizeForDriver, ref stFrameInfo, 1000);
            if (MyCamera.MV_OK != nRet)
            {
                ShowErrorMsg("No Data!", nRet);
                return;
            }

            MyCamera.MvGvspPixelType enDstPixelType;
            if (IsMonoData(stFrameInfo.enPixelType))
            {
                enDstPixelType = MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8;
            }
            else if (IsColorData(stFrameInfo.enPixelType))
            {
                enDstPixelType = MyCamera.MvGvspPixelType.PixelType_Gvsp_RGB8_Packed;
            }
            else
            {
                ShowErrorMsg("No such pixel type!", 0);
                return;
            }

            IntPtr pImage = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForSaveImage, 0);
            //MyCamera.MV_SAVE_IMAGE_PARAM_EX stSaveParam = new MyCamera.MV_SAVE_IMAGE_PARAM_EX();
            MyCamera.MV_PIXEL_CONVERT_PARAM stConverPixelParam = new MyCamera.MV_PIXEL_CONVERT_PARAM();
            stConverPixelParam.nWidth = stFrameInfo.nWidth;
            stConverPixelParam.nHeight = stFrameInfo.nHeight;
            stConverPixelParam.pSrcData = pData;
            stConverPixelParam.nSrcDataLen = stFrameInfo.nFrameLen;
            stConverPixelParam.enSrcPixelType = stFrameInfo.enPixelType;
            stConverPixelParam.enDstPixelType = enDstPixelType;
            stConverPixelParam.pDstBuffer = pImage;
            stConverPixelParam.nDstBufferSize = m_nBufSizeForSaveImage;
            nRet = m_pMyCamera.MV_CC_ConvertPixelType_NET(ref stConverPixelParam);
            if (MyCamera.MV_OK != nRet)
            {
                return;
            }

            if (enDstPixelType == MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8)
            {
                //************************Mono8 转 Bitmap*******************************
                Bitmap bmp = new Bitmap(stFrameInfo.nWidth, stFrameInfo.nHeight, stFrameInfo.nWidth * 1, PixelFormat.Format8bppIndexed, pImage);

                ColorPalette cp = bmp.Palette;
                // init palette
                for (int i = 0; i < 256; i++)
                {
                    cp.Entries[i] = Color.FromArgb(i, i, i);
                }
                // set palette back
                bmp.Palette = cp;

                bmp.Save("image1.bmp", ImageFormat.Bmp);
            }
            else
            {
                //*********************RGB8 转 Bitmap**************************
                for (int i = 0; i < stFrameInfo.nHeight; i++)
                {
                    for (int j = 0; j < stFrameInfo.nWidth; j++)
                    {
                        byte chRed = m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3];
                        m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3] = m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3 + 2];
                        m_pBufForSaveImage[i * stFrameInfo.nWidth * 3 + j * 3 + 2] = chRed;
                    }
                }
                try
                {
                    Bitmap bmp = new Bitmap(stFrameInfo.nWidth, stFrameInfo.nHeight, stFrameInfo.nWidth * 3, PixelFormat.Format24bppRgb, pImage);
                    bmp.Save("image1.bmp", ImageFormat.Bmp);
                }
                catch
                {
                }
            }
            ShowErrorMsg("Save Succeed!", 0);
        }

        private Boolean IsMonoData(MyCamera.MvGvspPixelType enGvspPixelType)
        {
            switch (enGvspPixelType)
            {
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono8:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono10:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono10_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono12:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_Mono12_Packed:
                    return true;

                default:
                    return false;
            }
        }

        private Boolean IsColorData(MyCamera.MvGvspPixelType enGvspPixelType)
        {
            switch (enGvspPixelType)
            {
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR8:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG8:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB8:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG8:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR10:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG10:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB10:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG10:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR12:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG12:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB12:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG12:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR10_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG10_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB10_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG10_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGR12_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerRG12_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerGB12_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_BayerBG12_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_RGB8_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_YUV422_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_YUV422_YUYV_Packed:
                case MyCamera.MvGvspPixelType.PixelType_Gvsp_YCBCR411_8_CBYYCRYY:
                    return true;

                default:
                    return false;
            }
        }

        #endregion 11.保存图片-Bmp

        #region 12.保存图片-jpg

        private void bnSaveJpg_Click(object sender, EventArgs e)
        {
            int nRet;
            UInt32 nPayloadSize = 0;
            MyCamera.MVCC_INTVALUE stParam = new MyCamera.MVCC_INTVALUE();
            nRet = m_pMyCamera.MV_CC_GetIntValue_NET("PayloadSize", ref stParam);
            if (MyCamera.MV_OK != nRet)
            {
                ShowErrorMsg("Get PayloadSize failed", nRet);
                return;
            }
            nPayloadSize = stParam.nCurValue;
            if (nPayloadSize > m_nBufSizeForDriver)
            {
                m_nBufSizeForDriver = nPayloadSize;
                m_pBufForDriver = new byte[m_nBufSizeForDriver];

                // ch:同时对保存图像的缓存做大小判断处理 | en:Determine the buffer size to save image
                // ch:BMP图片大小：width * height * 3 + 2048(预留BMP头大小) | en:BMP image size: width * height * 3 + 2048 (Reserved for BMP header)
                m_nBufSizeForSaveImage = m_nBufSizeForDriver * 3 + 2048;
                m_pBufForSaveImage = new byte[m_nBufSizeForSaveImage];
            }

            IntPtr pData = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForDriver, 0);
            MyCamera.MV_FRAME_OUT_INFO_EX stFrameInfo = new MyCamera.MV_FRAME_OUT_INFO_EX();

            // ch:超时获取一帧，超时时间为1秒 | en:Get one frame timeout, timeout is 1 sec
            nRet = m_pMyCamera.MV_CC_GetOneFrameTimeout_NET(pData, m_nBufSizeForDriver, ref stFrameInfo, 1000);
            if (MyCamera.MV_OK != nRet)
            {
                ShowErrorMsg("No Data!", nRet);
                return;
            }

            IntPtr pImage = Marshal.UnsafeAddrOfPinnedArrayElement(m_pBufForSaveImage, 0);
            MyCamera.MV_SAVE_IMAGE_PARAM_EX stSaveParam = new MyCamera.MV_SAVE_IMAGE_PARAM_EX();
            stSaveParam.enImageType = MyCamera.MV_SAVE_IAMGE_TYPE.MV_Image_Jpeg;
            stSaveParam.enPixelType = stFrameInfo.enPixelType;
            stSaveParam.pData = pData;
            stSaveParam.nDataLen = stFrameInfo.nFrameLen;
            stSaveParam.nHeight = stFrameInfo.nHeight;
            stSaveParam.nWidth = stFrameInfo.nWidth;
            stSaveParam.pImageBuffer = pImage;
            stSaveParam.nBufferSize = m_nBufSizeForSaveImage;
            stSaveParam.nJpgQuality = 80;
            nRet = m_pMyCamera.MV_CC_SaveImageEx_NET(ref stSaveParam);
            if (MyCamera.MV_OK != nRet)
            {
                ShowErrorMsg("Save Fail!", 0);
                return;
            }

            try
            {
                FileStream file = new FileStream("image.jpg", FileMode.Create, FileAccess.Write);
                file.Write(m_pBufForSaveImage, 0, (int)stSaveParam.nImageLen);
                file.Close();
            }
            catch
            {
            }
            ShowErrorMsg("Save Succeed!", 0);
        }

        #endregion 12.保存图片-jpg

        //获取相机参数
        private void bnGetParam_Click(object sender, EventArgs e)
        {
            MyCamera.MVCC_FLOATVALUE stParam = new MyCamera.MVCC_FLOATVALUE();
            int nRet = m_pMyCamera.MV_CC_GetFloatValue_NET("ExposureTime", ref stParam);
            if (MyCamera.MV_OK == nRet)
            {
                tbExposure.Text = stParam.fCurValue.ToString("F1");
            }

            nRet = m_pMyCamera.MV_CC_GetFloatValue_NET("ExposureTime", ref stParam);
            if (MyCamera.MV_OK == nRet)
            {
                m_pMyCamera.MV_CC_GetFloatValue_NET("Gain", ref stParam);
                tbGain.Text = stParam.fCurValue.ToString("F1");
            }

            nRet = m_pMyCamera.MV_CC_GetFloatValue_NET("ExposureTime", ref stParam);
            if (MyCamera.MV_OK == nRet)
            {
                m_pMyCamera.MV_CC_GetFloatValue_NET("ResultingFrameRate", ref stParam);
                tbFrameRate.Text = stParam.fCurValue.ToString("F1");
            }
        }

        //设置相机参数
        private void bnSetParam_Click(object sender, EventArgs e)
        {
            int nRet;
            m_pMyCamera.MV_CC_SetEnumValue_NET("ExposureAuto", 0);

            try
            {
                float.Parse(tbExposure.Text);
                float.Parse(tbGain.Text);
                float.Parse(tbFrameRate.Text);
            }
            catch
            {
                ShowErrorMsg("Please enter correct type!", 0);
                return;
            }

            nRet = m_pMyCamera.MV_CC_SetFloatValue_NET("ExposureTime", float.Parse(tbExposure.Text));
            if (nRet != MyCamera.MV_OK)
            {
                ShowErrorMsg("Set Exposure Time Fail!", nRet);
            }

            m_pMyCamera.MV_CC_SetEnumValue_NET("GainAuto", 0);
            nRet = m_pMyCamera.MV_CC_SetFloatValue_NET("Gain", float.Parse(tbGain.Text));
            if (nRet != MyCamera.MV_OK)
            {
                ShowErrorMsg("Set Gain Fail!", nRet);
            }

            nRet = m_pMyCamera.MV_CC_SetFloatValue_NET("AcquisitionFrameRate", float.Parse(tbFrameRate.Text));
            if (nRet != MyCamera.MV_OK)
            {
                ShowErrorMsg("Set Frame Rate Fail!", nRet);
            }
        }

        private string pathname = string.Empty;     		//定义路径名变量

        //加载图片
        private void LoadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            //ofd.Filter = "Excel文件(*.xls;*.xlsx)|*.xls;*.xlsx|所有文件|*.*";
            //ofd.InitialDirectory = ".";
            //ofd.Filter = "所有文件(*.*)|*.*";
            ofd.Filter = "图像文件(*.jpg;*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png";
            ofd.ValidateNames = true;
            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;

            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName != string.Empty)
            {
                try
                {
                    string pathname = ofd.FileName;
                    //this.pictureBox1.Load(pathname);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {
        }

        private void Button3_Click(object sender, EventArgs e)
        {
        }

        //private void LoadImage_Click(object sender, EventArgs e)
        //{
        //    //OpenFileDialog ofd = new OpenFileDialog();
        //    //ofd.Filter = "图像文件(*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png";
        //    //ofd.ValidateNames = true;
        //    //ofd.CheckFileExists = true;
        //    //ofd.CheckFileExists = true;
        //    //if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName != string.Empty)
        //    //{
        //    //    try
        //    //    {
        //    //        //获取图片路径
        //    //        string pathname = ofd.FileName;
        //    //        //加载到图片pictureBox中
        //    //        this.pictureBox1.Load(pathname);
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        MessageBox.Show(ex.Message);
        //    //    }
        //    //}

        //    HDevelopExport HD = new HDevelopExport();
        //    HD.RunHalcon(hWindowControl1.HalconWindow);
        //}

        public partial class HDevelopExport
        {
            public HTuple hv_ExpDefaultWinHandle;

            // Main procedure
            private void action()
            {
                // Local iconic variables

                HObject ho_Image1;

                // Local control variables

                HTuple hv_Width = null, hv_Height = null;
                // Initialize local and output iconic variables
                HOperatorSet.GenEmptyObj(out ho_Image1);
                //关闭窗口
                //dev_close_window(...);
                //读取图片 并显示出来
                ho_Image1.Dispose();
                HOperatorSet.ReadImage(out ho_Image1, "E:/个人资料/图片/031ec79e64f1e65e9c25cdcaeb8b8aaf.jpg");
                HOperatorSet.GetImageSize(ho_Image1, out hv_Width, out hv_Height);
                //dev_open_window(...);
                HOperatorSet.SetPart(hv_ExpDefaultWinHandle, 0, 0, hv_Height - 1, hv_Width - 1);
                HOperatorSet.DispObj(ho_Image1, hv_ExpDefaultWinHandle);

                ho_Image1.Dispose();
            }

            public void InitHalcon()
            {
                // Default settings used in HDevelop
                HOperatorSet.SetSystem("width", 512);
                HOperatorSet.SetSystem("height", 512);
            }

            public void RunHalcon(HTuple Window)
            {
                hv_ExpDefaultWinHandle = Window;
                action();
            }
        }

        //坐标值
        private static HTuple row = null;

        //二维码值
        private static HTuple QRCodeValue = null;

        //public HTuple hv_ExpDefaultWinHandle;

        //读取图片
        private void Button8_Click(object sender, EventArgs e)
        {
            HDevelopExport_6 hd = new HDevelopExport_6();
            hd.RunHalcon(hWindowControl1.HalconWindow);

            //二维码值
            //this.Coordinate_textBox.AppendText(row.ToString());

            //二维码值
            //Angle_textBox:角度值
            //this.QRCodeValue_textBox.AppendText(QRCodeValue.ToString());
        }

        /*

        /// <summary>
        /// halcon 识别多个二维码
        /// </summary>
        public partial class HDevelopExport_1
        {
            public HTuple hv_ExpDefaultWinHandle;

            // Main procedure
            private void action()
            {
                // Stack for temporary objects
                HObject[] OTemp = new HObject[20];

                // Local iconic variables

                HObject ho_Image, ho_GrayImage, ho_Region;
                HObject ho_ConnectedRegions, ho_SelectedRegions, ho_RegionTrans;
                HObject ho_RegionFillUp, ho_RegionDilation, ho_Rectangle1;
                HObject ho_RegionUnion, ho_ImageReduced1, ho_SortedRegions;
                HObject ho_EmptyObject, ho_ObjectSelected = null, ho_ImageReduced2 = null;
                HObject ho_SymbolXLDs = null;

                // Local control variables

                HTuple hv_WindowHandle = new HTuple(), hv_Area1 = null;
                HTuple hv_Row4 = null, hv_Column4 = null, hv_Number = null;
                HTuple hv_QRCode_information = null, hv_row_coord_tuple = null;
                HTuple hv_row_columns_tuple = null, hv_i = null, hv_Rows = new HTuple();
                HTuple hv_Columns = new HTuple(), hv_DataCodeHandle = new HTuple();
                HTuple hv_ResultHandles = new HTuple(), hv_DecodedDataStrings = new HTuple();
                // Initialize local and output iconic variables
                HOperatorSet.GenEmptyObj(out ho_Image);
                HOperatorSet.GenEmptyObj(out ho_GrayImage);
                HOperatorSet.GenEmptyObj(out ho_Region);
                HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
                HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
                HOperatorSet.GenEmptyObj(out ho_RegionTrans);
                HOperatorSet.GenEmptyObj(out ho_RegionFillUp);
                HOperatorSet.GenEmptyObj(out ho_RegionDilation);
                HOperatorSet.GenEmptyObj(out ho_Rectangle1);
                HOperatorSet.GenEmptyObj(out ho_RegionUnion);
                HOperatorSet.GenEmptyObj(out ho_ImageReduced1);
                HOperatorSet.GenEmptyObj(out ho_SortedRegions);
                HOperatorSet.GenEmptyObj(out ho_EmptyObject);
                HOperatorSet.GenEmptyObj(out ho_ObjectSelected);
                HOperatorSet.GenEmptyObj(out ho_ImageReduced2);
                HOperatorSet.GenEmptyObj(out ho_SymbolXLDs);
                //*一、为了提高二维码区域查找的准确性，必须利用Blob分析将查找的区域缩小
                //*由于图片是彩色的，不利于进行阈值分割，需要将RGB图像转化成灰度值图像
                //dev_close_window(...);
                //dev_open_window(...);
                ho_Image.Dispose();
                HOperatorSet.ReadImage(out ho_Image, "D:/1.工作资料/4.WIFI自动检测项目/3.视觉系统研发/wifi模组1.png");
                ho_GrayImage.Dispose();
                HOperatorSet.Rgb1ToGray(ho_Image, out ho_GrayImage);

                //draw_rectangle1 (WindowHandle, Row1, Column1, Row2, Column2)
                //gen_rectangle1 (Rectangle, Row1, Column1, Row2, Column2)
                //reduce_domain (GrayImage, Rectangle, ImageReduced)
                //将ImageReduced内阈值在130到255之间的区域筛选出来
                ho_Region.Dispose();
                HOperatorSet.Threshold(ho_GrayImage, out ho_Region, 163, 255);
                //将Region区域分离成一个个独立的区域
                ho_ConnectedRegions.Dispose();
                HOperatorSet.Connection(ho_Region, out ho_ConnectedRegions);
                //将ConnectedRegions区域内面积在9000到99999之间的区域筛选出来
                ho_SelectedRegions.Dispose();
                HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectedRegions, "area",
                    "and", 9000, 99999);
                //将SelectedRegions区域的形状转化成带倾斜角度的矩形区域
                ho_RegionTrans.Dispose();
                HOperatorSet.ShapeTrans(ho_SelectedRegions, out ho_RegionTrans, "rectangle2");
                //填充RegionTrans区域
                ho_RegionFillUp.Dispose();
                HOperatorSet.FillUp(ho_RegionTrans, out ho_RegionFillUp);

                //将RegionFillUp区域扩大30个像素点
                ho_RegionDilation.Dispose();
                HOperatorSet.DilationRectangle1(ho_RegionFillUp, out ho_RegionDilation, 30, 30);

                //查找RegionDilation区域内每个区域的面积和中心点坐标
                HOperatorSet.AreaCenter(ho_RegionDilation, out hv_Area1, out hv_Row4, out hv_Column4);

                //利用查找出来的中心点坐标画矩形
                ho_Rectangle1.Dispose();
                HOperatorSet.GenRectangle1(out ho_Rectangle1, hv_Row4 - 60, hv_Column4 - 105, hv_Row4 + 60,
                    hv_Column4 + 105);

                //**二、剔除了不必要区域的干扰后，就可以在理想的区域内进行二维码查找和内容解析
                //将Rectangle1区域结合成一个区域
                ho_RegionUnion.Dispose();
                HOperatorSet.Union1(ho_Rectangle1, out ho_RegionUnion);
                ho_ImageReduced1.Dispose();
                HOperatorSet.ReduceDomain(ho_GrayImage, ho_RegionUnion, out ho_ImageReduced1);
                ho_ConnectedRegions.Dispose();
                HOperatorSet.Connection(ho_ImageReduced1, out ho_ConnectedRegions);
                //计算出ConnectedRegions内区域的个数
                HOperatorSet.CountObj(ho_ConnectedRegions, out hv_Number);
                //将ConnectedRegions内的区域按照行排序，并且用数字代表相应的区域
                ho_SortedRegions.Dispose();
                HOperatorSet.SortRegion(ho_ConnectedRegions, out ho_SortedRegions, "first_point",
                    "true", "column");
                //建立一个空的项目
                ho_EmptyObject.Dispose();
                HOperatorSet.GenEmptyObj(out ho_EmptyObject);

                //二维码对象
                hv_QRCode_information = 0;

                //坐标对象
                hv_row_coord_tuple = new HTuple();
                hv_row_columns_tuple = new HTuple();

                HTuple end_val52 = hv_Number;
                HTuple step_val52 = 1;
                for (hv_i = 1; hv_i.Continue(end_val52, step_val52); hv_i = hv_i.TupleAdd(step_val52))
                {
                    //根据i的数值选择SortedRegions内排序的区域
                    ho_ObjectSelected.Dispose();
                    HOperatorSet.SelectObj(ho_SortedRegions, out ho_ObjectSelected, hv_i);

                    //获取region的坐标
                    HOperatorSet.GetRegionConvex(ho_ObjectSelected, out hv_Rows, out hv_Columns);

                    //get_contour_xld (ObjectSelected, Row, Col)

                    //find_shape_model (ObjectSelected, WindowHandle, -0.39, 0.79, 0.5, 1, 0.5, 'least_squares', 0, 0.9, Row, Column, Angle, Score)

                    //gen_cross_contour_xld (Cross, Row, Column, 6, 0.785398)

                    hv_row_coord_tuple = hv_Rows.Clone();
                    hv_row_columns_tuple = hv_Columns.Clone();

                    row = hv_row_coord_tuple;

                    ho_ImageReduced2.Dispose();
                    HOperatorSet.ReduceDomain(ho_GrayImage, ho_ObjectSelected, out ho_ImageReduced2);
                    //建立QR Code类型的二维码模型句柄
                    HOperatorSet.CreateDataCode2dModel("QR Code", new HTuple(), new HTuple(), out hv_DataCodeHandle);
                    //在ImageReduced2内查找二维码轮廓和解析其内容
                    ho_SymbolXLDs.Dispose();
                    HOperatorSet.FindDataCode2d(ho_ImageReduced2, out ho_SymbolXLDs, hv_DataCodeHandle,
                        "train", "all", out hv_ResultHandles, out hv_DecodedDataStrings);
                    if (hv_QRCode_information == null)
                        hv_QRCode_information = new HTuple();
                    hv_QRCode_information[hv_i - 1] = hv_DecodedDataStrings;

                    {
                        HObject ExpTmpOutVar_0;
                        HOperatorSet.ConcatObj(ho_SymbolXLDs, ho_EmptyObject, out ExpTmpOutVar_0);
                        ho_EmptyObject.Dispose();
                        ho_EmptyObject = ExpTmpOutVar_0;
                    }

                    QRCodeValue = hv_QRCode_information;
                }
                HOperatorSet.SetColor(hv_ExpDefaultWinHandle, "green");
                HOperatorSet.DispObj(ho_GrayImage, hv_ExpDefaultWinHandle);
                HOperatorSet.DispObj(ho_EmptyObject, hv_ExpDefaultWinHandle);
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(2)) - 50,
                    (hv_Column4.TupleSelect(2)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(0)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(5)) - 50, (hv_Column4.TupleSelect(5)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(1)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(1)) - 50, (hv_Column4.TupleSelect(1)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(2)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(4)) - 50, (hv_Column4.TupleSelect(4)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(3)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(0)) - 50,
                    (hv_Column4.TupleSelect(0)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    4)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(3)) - 50,
                    (hv_Column4.TupleSelect(3)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    5)));
                ho_Image.Dispose();
                ho_GrayImage.Dispose();
                ho_Region.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectedRegions.Dispose();
                ho_RegionTrans.Dispose();
                ho_RegionFillUp.Dispose();
                ho_RegionDilation.Dispose();
                ho_Rectangle1.Dispose();
                ho_RegionUnion.Dispose();
                ho_ImageReduced1.Dispose();
                ho_SortedRegions.Dispose();
                ho_EmptyObject.Dispose();
                ho_ObjectSelected.Dispose();
                ho_ImageReduced2.Dispose();
                ho_SymbolXLDs.Dispose();
            }

            public void InitHalcon()
            {
                // Default settings used in HDevelop
                HOperatorSet.SetSystem("width", 512);
                HOperatorSet.SetSystem("height", 512);
            }

            public void RunHalcon(HTuple Window)
            {
                hv_ExpDefaultWinHandle = Window;
                action();
            }
        }

    */

        //计算角度使用halcon窗口

        /*
        public partial class HDevelopExport_2
        {
            public HTuple hv_ExpDefaultWinHandle;

            // Main procedure
            private void action()
            {
                // Stack for temporary objects
                HObject[] OTemp = new HObject[20];

                // Local iconic variables

                HObject ho_Image, ho_GrayImage, ho_Region;
                HObject ho_ConnectedRegions, ho_SelectedRegions, ho_RegionTrans;
                HObject ho_RegionFillUp, ho_RegionDilation, ho_Rectangle1;
                HObject ho_RegionUnion, ho_ImageReduced1, ho_SortedRegions;
                HObject ho_EmptyObject, ho_ObjectSelected = null, ho_ImageReduced2 = null;
                HObject ho_SymbolXLDs = null;

                // Local control variables

                HTuple hv_WindowHandle = new HTuple(), hv_Width = null;
                HTuple hv_Height = null, hv_Area1 = null, hv_Row4 = null;
                HTuple hv_Column4 = null, hv_Phi = null, hv_angle = null;
                HTuple hv_Number = null, hv_QRCode_information = null;
                HTuple hv_i = null, hv_DataCodeHandle = new HTuple(), hv_ResultHandles = new HTuple();
                HTuple hv_DecodedDataStrings = new HTuple();
                // Initialize local and output iconic variables
                HOperatorSet.GenEmptyObj(out ho_Image);
                HOperatorSet.GenEmptyObj(out ho_GrayImage);
                HOperatorSet.GenEmptyObj(out ho_Region);
                HOperatorSet.GenEmptyObj(out ho_ConnectedRegions);
                HOperatorSet.GenEmptyObj(out ho_SelectedRegions);
                HOperatorSet.GenEmptyObj(out ho_RegionTrans);
                HOperatorSet.GenEmptyObj(out ho_RegionFillUp);
                HOperatorSet.GenEmptyObj(out ho_RegionDilation);
                HOperatorSet.GenEmptyObj(out ho_Rectangle1);
                HOperatorSet.GenEmptyObj(out ho_RegionUnion);
                HOperatorSet.GenEmptyObj(out ho_ImageReduced1);
                HOperatorSet.GenEmptyObj(out ho_SortedRegions);
                HOperatorSet.GenEmptyObj(out ho_EmptyObject);
                HOperatorSet.GenEmptyObj(out ho_ObjectSelected);
                HOperatorSet.GenEmptyObj(out ho_ImageReduced2);
                HOperatorSet.GenEmptyObj(out ho_SymbolXLDs);
                //*一、为了提高二维码区域查找的准确性，必须利用Blob分析将查找的区域缩小
                //*由于图片是彩色的，不利于进行阈值分割，需要将RGB图像转化成灰度值图像
                //dev_close_window(...);
                //dev_open_window(...);
                ho_Image.Dispose();
                HOperatorSet.ReadImage(out ho_Image, "wifi模组1.png");
                HOperatorSet.GetImageSize(ho_Image, out hv_Width, out hv_Height);
                ho_GrayImage.Dispose();
                HOperatorSet.Rgb1ToGray(ho_Image, out ho_GrayImage);

                //draw_rectangle1 (WindowHandle, Row1, Column1, Row2, Column2)
                //gen_rectangle1 (Rectangle, Row1, Column1, Row2, Column2)
                //reduce_domain (GrayImage, Rectangle, ImageReduced)
                //将ImageReduced内阈值在130到255之间的区域筛选出来
                ho_Region.Dispose();
                HOperatorSet.Threshold(ho_GrayImage, out ho_Region, 163, 255);
                //将Region区域分离成一个个独立的区域
                ho_ConnectedRegions.Dispose();
                HOperatorSet.Connection(ho_Region, out ho_ConnectedRegions);
                //将ConnectedRegions区域内面O积在9000到99999之间的区域筛选出来
                ho_SelectedRegions.Dispose();
                HOperatorSet.SelectShape(ho_ConnectedRegions, out ho_SelectedRegions, "area",
                    "and", 9000, 99999);
                //将SelectedRegions区域的形状转化成带倾斜角度的矩形区域
                ho_RegionTrans.Dispose();
                HOperatorSet.ShapeTrans(ho_SelectedRegions, out ho_RegionTrans, "rectangle2");
                //填充RegionTrans区域
                ho_RegionFillUp.Dispose();
                HOperatorSet.FillUp(ho_RegionTrans, out ho_RegionFillUp);

                //将RegionFillUp区域扩大30个像素点
                ho_RegionDilation.Dispose();
                HOperatorSet.DilationRectangle1(ho_RegionFillUp, out ho_RegionDilation, 30, 30);
                //查找RegionDilation区域内每个区域的面积和中心点坐标
                HOperatorSet.AreaCenter(ho_RegionDilation, out hv_Area1, out hv_Row4, out hv_Column4);

                //计算每个区域的角度
                HOperatorSet.OrientationRegion(ho_RegionDilation, out hv_Phi);
                hv_angle = hv_Phi.TupleDeg();

                //利用查找出来的中心点坐标画矩形
                ho_Rectangle1.Dispose();
                HOperatorSet.GenRectangle1(out ho_Rectangle1, hv_Row4 - 60, hv_Column4 - 105, hv_Row4 + 60,
                    hv_Column4 + 105);

                //**二、剔除了不必要区域的干扰后，就可以在理想的区域内进行二维码查找和内容解析
                //将Rectangle1区域结合成一个区域
                ho_RegionUnion.Dispose();
                HOperatorSet.Union1(ho_Rectangle1, out ho_RegionUnion);
                ho_ImageReduced1.Dispose();
                HOperatorSet.ReduceDomain(ho_GrayImage, ho_RegionUnion, out ho_ImageReduced1);
                ho_ConnectedRegions.Dispose();
                HOperatorSet.Connection(ho_ImageReduced1, out ho_ConnectedRegions);
                //计算出ConnectedRegions内区域的个数
                HOperatorSet.CountObj(ho_ConnectedRegions, out hv_Number);
                //将ConnectedRegions内的区域按照行排序，并且用数字代表相应的区域
                ho_SortedRegions.Dispose();
                HOperatorSet.SortRegion(ho_ConnectedRegions, out ho_SortedRegions, "first_point",
                    "true", "column");
                //建立一个空的项目
                ho_EmptyObject.Dispose();
                HOperatorSet.GenEmptyObj(out ho_EmptyObject);
                hv_QRCode_information = 0;
                HTuple end_val46 = hv_Number;
                HTuple step_val46 = 1;
                for (hv_i = 1; hv_i.Continue(end_val46, step_val46); hv_i = hv_i.TupleAdd(step_val46))
                {
                    //根据i的数值选择SortedRegions内排序的区域
                    ho_ObjectSelected.Dispose();
                    HOperatorSet.SelectObj(ho_SortedRegions, out ho_ObjectSelected, hv_i);
                    ho_ImageReduced2.Dispose();
                    HOperatorSet.ReduceDomain(ho_GrayImage, ho_ObjectSelected, out ho_ImageReduced2
                        );
                    //建立QR Code类型的二维码模型句柄
                    HOperatorSet.CreateDataCode2dModel("QR Code", new HTuple(), new HTuple(), out hv_DataCodeHandle);
                    //在ImageReduced2内查找二维码轮廓和解析其内容
                    ho_SymbolXLDs.Dispose();
                    HOperatorSet.FindDataCode2d(ho_ImageReduced2, out ho_SymbolXLDs, hv_DataCodeHandle,
                        "train", "all", out hv_ResultHandles, out hv_DecodedDataStrings);
                    if (hv_QRCode_information == null)
                        hv_QRCode_information = new HTuple();
                    hv_QRCode_information[hv_i - 1] = hv_DecodedDataStrings;
                    {
                        HObject ExpTmpOutVar_0;
                        HOperatorSet.ConcatObj(ho_SymbolXLDs, ho_EmptyObject, out ExpTmpOutVar_0);
                        ho_EmptyObject.Dispose();
                        ho_EmptyObject = ExpTmpOutVar_0;
                    }
                }
                HOperatorSet.SetColor(hv_ExpDefaultWinHandle, "green");
                HOperatorSet.DispObj(ho_GrayImage, hv_ExpDefaultWinHandle);
                HOperatorSet.DispObj(ho_EmptyObject, hv_ExpDefaultWinHandle);
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(2)) - 50,
                    (hv_Column4.TupleSelect(2)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    0)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(5)) - 50,
                    (hv_Column4.TupleSelect(5)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    1)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(1)) - 50,
                    (hv_Column4.TupleSelect(1)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    2)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(4)) - 50,
                    (hv_Column4.TupleSelect(4)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    3)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(0)) - 50,
                    (hv_Column4.TupleSelect(0)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    4)));
                HOperatorSet.SetTposition(hv_ExpDefaultWinHandle, (hv_Row4.TupleSelect(3)) - 50,
                    (hv_Column4.TupleSelect(3)) - 10);
                HOperatorSet.WriteString(hv_ExpDefaultWinHandle, "二维码信息：" + (hv_QRCode_information.TupleSelect(
                    5)));
                ho_Image.Dispose();
                ho_GrayImage.Dispose();
                ho_Region.Dispose();
                ho_ConnectedRegions.Dispose();
                ho_SelectedRegions.Dispose();
                ho_RegionTrans.Dispose();
                ho_RegionFillUp.Dispose();
                ho_RegionDilation.Dispose();
                ho_Rectangle1.Dispose();
                ho_RegionUnion.Dispose();
                ho_ImageReduced1.Dispose();
                ho_SortedRegions.Dispose();
                ho_EmptyObject.Dispose();
                ho_ObjectSelected.Dispose();
                ho_ImageReduced2.Dispose();
                ho_SymbolXLDs.Dispose();
            }

            public void InitHalcon()
            {
                // Default settings used in HDevelop
                HOperatorSet.SetSystem("width", 838);
                HOperatorSet.SetSystem("height", 643);
            }

            public void RunHalcon(HTuple Window)
            {
                hv_ExpDefaultWinHandle = Window;
                action();
            }
        }

        */
    }
}