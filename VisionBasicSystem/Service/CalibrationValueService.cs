﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionBasicSystem.Dal;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Service
{
    internal class CalibrationValueService
    {
        private CalibrationValueDal valueDal = new CalibrationValueDal();

        //根据编号查询标定值
        public CalibrationValue GetCalibrationValue()
        {
            return valueDal.GetCalibrationValue();
        }

        //修改标定值
        public bool UpdateCalibrationValue(CalibrationValue calibration)
        {
            return valueDal.UpdateCalibrationValue(calibration) > 0;
        }
    }
}