﻿using System.Collections.Generic;
using VisionBasicSystem.Dal;

namespace VisionBasicSystem.Service
{
    public class UserInfoService
    {
        public UserInfoDal userInfoDal = new UserInfoDal();

        public List<UserInfo> GetLis()
        {
            return userInfoDal.GetList();
        }
    }
}