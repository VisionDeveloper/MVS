﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionBasicSystem.Dal;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Service
{
    public class ImageDisposeDataService
    {
        private ImageDisposeDataDal ImageDisposeData = new ImageDisposeDataDal();

        //根据ID获取相机处理数据
        public ImageDisposeData GetImageData()
        {
            return ImageDisposeData.GetImageData();
        }

        //修改锡点值
        public bool EditTinValue(ImageDisposeData imageDispose)
        {
            return ImageDisposeData.EditTinValue(imageDispose) > 0;
        }

        //修改锡点值
        public bool EditOffset(ImageDisposeData imageDispose)
        {
            return ImageDisposeData.EditOffset(imageDispose) > 0;
        }

        //更新标定坐标
        public bool UpdateCalibrationValue(ImageDisposeData imageDispose)
        {
            return ImageDisposeData.UpdateCalibrationValue(imageDispose) > 0;
        }
    }
}