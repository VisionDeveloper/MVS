﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionBasicSystem.Dal;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Service
{
    public class RoiValueService
    {
        private RoiValueDal valueDal = new RoiValueDal();

        //根据编号查询ROI值
        public RoiValue GetRoiValue()
        {
            return valueDal.GetRoiValue();
        }

        //修改标定值
        public bool UpdateRoiValue(RoiValue roiValue)
        {
            return valueDal.UpdateRoiValue(roiValue) > 0;
        }
    }
}