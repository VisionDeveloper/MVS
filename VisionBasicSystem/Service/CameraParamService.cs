﻿using HalconDotNet;
using MvCamCtrl.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VisionBasicSystem.Dal;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Service
{
    //相机参数相关方法
    public class CameraParamService
    {
        private CameraParamDal paramDal = new CameraParamDal();

        //根据编号查询相机参数
        public CameraParam GetCameraParam()
        {
            return paramDal.GetCameraParam();
        }

        //修改相机参数SetCameraParam
        public bool SetCameraParam(CameraParam cameraParam)
        {
            return paramDal.SetCameraParam(cameraParam) > 0;
        }
    }
}