﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem.Model
{
    //处理图片数据
    public class ImageDisposeData
    {
        //主键ID
        public int Id { get; set; }

        //偏移点1x坐标
        public decimal X1offset { get; set; }

        //偏移点2x坐标
        public decimal X2offset { get; set; }

        //偏移点3x坐标
        public decimal X3offset { get; set; }

        //偏移点4x坐标
        public decimal X4offset { get; set; }

        //偏移点1y坐标
        public decimal Y1offset { get; set; }

        //偏移点2y坐标
        public decimal Y2offset { get; set; }

        //偏移点3y坐标
        public decimal Y3offset { get; set; }

        //偏移点4y坐标
        public decimal Y4offset { get; set; }

        //锡点值
        public int TinValue { get; set; }

        //激光点1,x坐标
        public decimal Laser1x { get; set; }

        //标定点1,y坐标
        public decimal Laser1y { get; set; }

        //标定点2,x坐标
        public decimal Laser2x { get; set; }

        //标定点2,y坐标
        public decimal Laser2y { get; set; }

        //标定点3,x坐标
        public decimal Laser3x { get; set; }

        //标定点3,y坐标
        public decimal Laser3y { get; set; }

        //标定点4,x坐标
        public decimal Laser4x { get; set; }

        //标定点4,y坐标
        public decimal Laser4y { get; set; }
    }
}