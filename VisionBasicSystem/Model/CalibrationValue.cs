﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem.Model
{
    //标定数据
    public class CalibrationValue
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}