﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem.Model
{
    //相机参数
    public class CameraParam
    {
        public int Id { get; set; }

        //曝光
        public int Exposure { get; set; }

        //增益
        public decimal Gain { get; set; }

        //帧率
        public int FrameRate { get; set; }
    }
}