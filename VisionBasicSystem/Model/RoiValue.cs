﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem.Model
{
    public class RoiValue
    {
        public int Id;
        public decimal Row1;
        public decimal Column1;
        public decimal Row2;
        public decimal Column2;
    }
}