﻿using HalconDotNet;
using System;
using VisionBasicSystem.Commons;

namespace VisionBasicSystem
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
           
            try
            {
                base.Dispose(disposing);
            }
            catch (Exception e)
            {
                LogHelper.WriteLog("释放资源失败", e);
            }

        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bnClose = new System.Windows.Forms.Button();
            this.bnOpen = new System.Windows.Forms.Button();
            this.bnEnum = new System.Windows.Forms.Button();
            this.cbDeviceList = new System.Windows.Forms.ComboBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.bnSaveJpg = new System.Windows.Forms.Button();
            this.bnSaveBmp = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.bnSetParam = new System.Windows.Forms.Button();
            this.bnGetParam = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.tbFrameRate = new System.Windows.Forms.TextBox();
            this.tbGain = new System.Windows.Forms.TextBox();
            this.tbExposure = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.bnTriggerExec = new System.Windows.Forms.Button();
            this.cbSoftTrigger = new System.Windows.Forms.CheckBox();
            this.bnStopGrab = new System.Windows.Forms.Button();
            this.bnStartGrab = new System.Windows.Forms.Button();
            this.bnTriggerMode = new System.Windows.Forms.RadioButton();
            this.bnContinuesMode = new System.Windows.Forms.RadioButton();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.Point4_Txt = new System.Windows.Forms.TextBox();
            this.Point3_Txt = new System.Windows.Forms.TextBox();
            this.Point2_Txt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Point1_Txt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.TinValue_Btn = new System.Windows.Forms.Button();
            this.TinValue = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Demarcate_Btn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Qy_Row1 = new System.Windows.Forms.NumericUpDown();
            this.Qx_Column1 = new System.Windows.Forms.NumericUpDown();
            this.Qy_Row4 = new System.Windows.Forms.NumericUpDown();
            this.Qx_Column4 = new System.Windows.Forms.NumericUpDown();
            this.Qy_Row3 = new System.Windows.Forms.NumericUpDown();
            this.Qx_Column3 = new System.Windows.Forms.NumericUpDown();
            this.Qy_Row2 = new System.Windows.Forms.NumericUpDown();
            this.Qx_Column2 = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Py_Row4 = new System.Windows.Forms.NumericUpDown();
            this.Px_Column4 = new System.Windows.Forms.NumericUpDown();
            this.Py_Row3 = new System.Windows.Forms.NumericUpDown();
            this.Px_Column3 = new System.Windows.Forms.NumericUpDown();
            this.Py_Row2 = new System.Windows.Forms.NumericUpDown();
            this.Px_Column1 = new System.Windows.Forms.NumericUpDown();
            this.Px_Column2 = new System.Windows.Forms.NumericUpDown();
            this.Py_Row1 = new System.Windows.Forms.NumericUpDown();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.SaveOffset_Btn = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.Y4offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.Y3offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.Y2offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.Y1offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.X4offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.X3offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.X2offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.X1offset_Nud = new System.Windows.Forms.NumericUpDown();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.roi_Column1 = new System.Windows.Forms.NumericUpDown();
            this.Roi_Save_Btn = new System.Windows.Forms.Button();
            this.roi_Row1 = new System.Windows.Forms.NumericUpDown();
            this.roi_Column2 = new System.Windows.Forms.NumericUpDown();
            this.roi_Row2 = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Model_Save_Btn = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.CreateMode_Button = new System.Windows.Forms.Button();
            this.Product_Comb = new System.Windows.Forms.ComboBox();
            this.button17 = new System.Windows.Forms.Button();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.label48 = new System.Windows.Forms.Label();
            this.rtbMySendMessage = new System.Windows.Forms.TextBox();
            this.rtbReceiveMsg = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txbServerPort = new System.Windows.Forms.TextBox();
            this.txbServerIP = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Date_Lab = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row1)).BeginInit();
            this.tabPage8.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Y4offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y3offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y2offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y1offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X4offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X3offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2offset_Nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.X1offset_Nud)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Column1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Row1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Column2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Row2)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bnClose);
            this.groupBox1.Controls.Add(this.bnOpen);
            this.groupBox1.Controls.Add(this.bnEnum);
            this.groupBox1.Location = new System.Drawing.Point(9, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(324, 127);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "初始化";
            // 
            // bnClose
            // 
            this.bnClose.Enabled = false;
            this.bnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnClose.Location = new System.Drawing.Point(190, 71);
            this.bnClose.Margin = new System.Windows.Forms.Padding(4);
            this.bnClose.Name = "bnClose";
            this.bnClose.Size = new System.Drawing.Size(117, 38);
            this.bnClose.TabIndex = 2;
            this.bnClose.Text = "关闭设备";
            this.bnClose.UseVisualStyleBackColor = true;
            this.bnClose.Click += new System.EventHandler(this.bnClose_Click);
            // 
            // bnOpen
            // 
            this.bnOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnOpen.Location = new System.Drawing.Point(27, 71);
            this.bnOpen.Margin = new System.Windows.Forms.Padding(4);
            this.bnOpen.Name = "bnOpen";
            this.bnOpen.Size = new System.Drawing.Size(113, 38);
            this.bnOpen.TabIndex = 1;
            this.bnOpen.Text = "打开设备";
            this.bnOpen.UseVisualStyleBackColor = true;
            this.bnOpen.Click += new System.EventHandler(this.bnOpen_Click);
            // 
            // bnEnum
            // 
            this.bnEnum.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnEnum.Location = new System.Drawing.Point(27, 25);
            this.bnEnum.Margin = new System.Windows.Forms.Padding(4);
            this.bnEnum.Name = "bnEnum";
            this.bnEnum.Size = new System.Drawing.Size(280, 38);
            this.bnEnum.TabIndex = 0;
            this.bnEnum.Text = "查找设备";
            this.bnEnum.UseVisualStyleBackColor = true;
            this.bnEnum.Click += new System.EventHandler(this.BnEnum_Click);
            // 
            // cbDeviceList
            // 
            this.cbDeviceList.FormattingEnabled = true;
            this.cbDeviceList.Location = new System.Drawing.Point(13, 18);
            this.cbDeviceList.Margin = new System.Windows.Forms.Padding(4);
            this.cbDeviceList.Name = "cbDeviceList";
            this.cbDeviceList.Size = new System.Drawing.Size(885, 23);
            this.cbDeviceList.TabIndex = 3;
            // 
            // tabControl2
            // 
            this.tabControl2.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl2.Location = new System.Drawing.Point(905, 58);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(416, 689);
            this.tabControl2.TabIndex = 6;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox5);
            this.tabPage6.Controls.Add(this.groupBox10);
            this.tabPage6.Controls.Add(this.groupBox4);
            this.tabPage6.Controls.Add(this.groupBox1);
            this.tabPage6.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage6.Location = new System.Drawing.Point(4, 4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(379, 681);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "相机";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.bnSaveJpg);
            this.groupBox5.Controls.Add(this.bnSaveBmp);
            this.groupBox5.Location = new System.Drawing.Point(9, 313);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(324, 71);
            this.groupBox5.TabIndex = 47;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "保存图片";
            // 
            // bnSaveJpg
            // 
            this.bnSaveJpg.Enabled = false;
            this.bnSaveJpg.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnSaveJpg.Location = new System.Drawing.Point(190, 25);
            this.bnSaveJpg.Margin = new System.Windows.Forms.Padding(4);
            this.bnSaveJpg.Name = "bnSaveJpg";
            this.bnSaveJpg.Size = new System.Drawing.Size(113, 38);
            this.bnSaveJpg.TabIndex = 0;
            this.bnSaveJpg.Text = "保存JPG";
            this.bnSaveJpg.UseVisualStyleBackColor = true;
            this.bnSaveJpg.Click += new System.EventHandler(this.bnSaveJpg_Click);
            // 
            // bnSaveBmp
            // 
            this.bnSaveBmp.Enabled = false;
            this.bnSaveBmp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnSaveBmp.Location = new System.Drawing.Point(27, 25);
            this.bnSaveBmp.Margin = new System.Windows.Forms.Padding(4);
            this.bnSaveBmp.Name = "bnSaveBmp";
            this.bnSaveBmp.Size = new System.Drawing.Size(113, 38);
            this.bnSaveBmp.TabIndex = 0;
            this.bnSaveBmp.Text = "保存BMP";
            this.bnSaveBmp.UseVisualStyleBackColor = true;
            this.bnSaveBmp.Click += new System.EventHandler(this.bnSaveBmp_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.bnSetParam);
            this.groupBox10.Controls.Add(this.bnGetParam);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.label15);
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Controls.Add(this.tbFrameRate);
            this.groupBox10.Controls.Add(this.tbGain);
            this.groupBox10.Controls.Add(this.tbExposure);
            this.groupBox10.Location = new System.Drawing.Point(9, 477);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox10.Size = new System.Drawing.Size(324, 191);
            this.groupBox10.TabIndex = 48;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "参数";
            // 
            // bnSetParam
            // 
            this.bnSetParam.Enabled = false;
            this.bnSetParam.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnSetParam.Location = new System.Drawing.Point(183, 138);
            this.bnSetParam.Margin = new System.Windows.Forms.Padding(4);
            this.bnSetParam.Name = "bnSetParam";
            this.bnSetParam.Size = new System.Drawing.Size(113, 38);
            this.bnSetParam.TabIndex = 7;
            this.bnSetParam.Text = "设置参数";
            this.bnSetParam.UseVisualStyleBackColor = true;
            this.bnSetParam.Click += new System.EventHandler(this.bnSetParam_Click);
            // 
            // bnGetParam
            // 
            this.bnGetParam.Enabled = false;
            this.bnGetParam.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnGetParam.Location = new System.Drawing.Point(24, 138);
            this.bnGetParam.Margin = new System.Windows.Forms.Padding(4);
            this.bnGetParam.Name = "bnGetParam";
            this.bnGetParam.Size = new System.Drawing.Size(113, 38);
            this.bnGetParam.TabIndex = 6;
            this.bnGetParam.Text = "获取参数";
            this.bnGetParam.UseVisualStyleBackColor = true;
            this.bnGetParam.Click += new System.EventHandler(this.bnGetParam_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label14.Location = new System.Drawing.Point(60, 103);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 24);
            this.label14.TabIndex = 5;
            this.label14.Text = "帧率：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(60, 64);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(64, 24);
            this.label15.TabIndex = 4;
            this.label15.Text = "增益：";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label36.Location = new System.Drawing.Point(60, 21);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(64, 24);
            this.label36.TabIndex = 3;
            this.label36.Text = "曝光：";
            // 
            // tbFrameRate
            // 
            this.tbFrameRate.Enabled = false;
            this.tbFrameRate.Location = new System.Drawing.Point(179, 100);
            this.tbFrameRate.Margin = new System.Windows.Forms.Padding(4);
            this.tbFrameRate.Name = "tbFrameRate";
            this.tbFrameRate.Size = new System.Drawing.Size(132, 30);
            this.tbFrameRate.TabIndex = 2;
            // 
            // tbGain
            // 
            this.tbGain.Enabled = false;
            this.tbGain.Location = new System.Drawing.Point(179, 61);
            this.tbGain.Margin = new System.Windows.Forms.Padding(4);
            this.tbGain.Name = "tbGain";
            this.tbGain.Size = new System.Drawing.Size(132, 30);
            this.tbGain.TabIndex = 1;
            // 
            // tbExposure
            // 
            this.tbExposure.Enabled = false;
            this.tbExposure.Location = new System.Drawing.Point(179, 18);
            this.tbExposure.Margin = new System.Windows.Forms.Padding(4);
            this.tbExposure.Name = "tbExposure";
            this.tbExposure.Size = new System.Drawing.Size(132, 30);
            this.tbExposure.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.bnTriggerExec);
            this.groupBox4.Controls.Add(this.cbSoftTrigger);
            this.groupBox4.Controls.Add(this.bnStopGrab);
            this.groupBox4.Controls.Add(this.bnStartGrab);
            this.groupBox4.Controls.Add(this.bnTriggerMode);
            this.groupBox4.Controls.Add(this.bnContinuesMode);
            this.groupBox4.Location = new System.Drawing.Point(9, 142);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(324, 163);
            this.groupBox4.TabIndex = 46;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "采集图像";
            // 
            // bnTriggerExec
            // 
            this.bnTriggerExec.Enabled = false;
            this.bnTriggerExec.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnTriggerExec.Location = new System.Drawing.Point(191, 115);
            this.bnTriggerExec.Margin = new System.Windows.Forms.Padding(4);
            this.bnTriggerExec.Name = "bnTriggerExec";
            this.bnTriggerExec.Size = new System.Drawing.Size(123, 38);
            this.bnTriggerExec.TabIndex = 5;
            this.bnTriggerExec.Text = "软触发一次";
            this.bnTriggerExec.UseVisualStyleBackColor = true;
            this.bnTriggerExec.Click += new System.EventHandler(this.bnTriggerExec_Click);
            // 
            // cbSoftTrigger
            // 
            this.cbSoftTrigger.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSoftTrigger.AutoSize = true;
            this.cbSoftTrigger.Enabled = false;
            this.cbSoftTrigger.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbSoftTrigger.Location = new System.Drawing.Point(38, 121);
            this.cbSoftTrigger.Margin = new System.Windows.Forms.Padding(4);
            this.cbSoftTrigger.Name = "cbSoftTrigger";
            this.cbSoftTrigger.Size = new System.Drawing.Size(86, 28);
            this.cbSoftTrigger.TabIndex = 4;
            this.cbSoftTrigger.Text = "软触发";
            this.cbSoftTrigger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cbSoftTrigger.UseVisualStyleBackColor = true;
            this.cbSoftTrigger.CheckedChanged += new System.EventHandler(this.cbSoftTrigger_CheckedChanged);
            // 
            // bnStopGrab
            // 
            this.bnStopGrab.Enabled = false;
            this.bnStopGrab.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnStopGrab.Location = new System.Drawing.Point(188, 57);
            this.bnStopGrab.Margin = new System.Windows.Forms.Padding(4);
            this.bnStopGrab.Name = "bnStopGrab";
            this.bnStopGrab.Size = new System.Drawing.Size(123, 38);
            this.bnStopGrab.TabIndex = 3;
            this.bnStopGrab.Text = "停止采集";
            this.bnStopGrab.UseVisualStyleBackColor = true;
            this.bnStopGrab.Click += new System.EventHandler(this.bnStopGrab_Click);
            // 
            // bnStartGrab
            // 
            this.bnStartGrab.Enabled = false;
            this.bnStartGrab.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnStartGrab.Location = new System.Drawing.Point(24, 57);
            this.bnStartGrab.Margin = new System.Windows.Forms.Padding(4);
            this.bnStartGrab.Name = "bnStartGrab";
            this.bnStartGrab.Size = new System.Drawing.Size(113, 38);
            this.bnStartGrab.TabIndex = 2;
            this.bnStartGrab.Text = "开始采集";
            this.bnStartGrab.UseVisualStyleBackColor = true;
            this.bnStartGrab.Click += new System.EventHandler(this.bnStartGrab_Click);
            // 
            // bnTriggerMode
            // 
            this.bnTriggerMode.AutoSize = true;
            this.bnTriggerMode.Enabled = false;
            this.bnTriggerMode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnTriggerMode.Location = new System.Drawing.Point(188, 22);
            this.bnTriggerMode.Margin = new System.Windows.Forms.Padding(4);
            this.bnTriggerMode.Name = "bnTriggerMode";
            this.bnTriggerMode.Size = new System.Drawing.Size(103, 28);
            this.bnTriggerMode.TabIndex = 1;
            this.bnTriggerMode.TabStop = true;
            this.bnTriggerMode.Text = "触发模式";
            this.bnTriggerMode.UseMnemonic = false;
            this.bnTriggerMode.UseVisualStyleBackColor = true;
            this.bnTriggerMode.CheckedChanged += new System.EventHandler(this.bnTriggerMode_CheckedChanged);
            // 
            // bnContinuesMode
            // 
            this.bnContinuesMode.AutoSize = true;
            this.bnContinuesMode.Enabled = false;
            this.bnContinuesMode.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bnContinuesMode.Location = new System.Drawing.Point(24, 22);
            this.bnContinuesMode.Margin = new System.Windows.Forms.Padding(4);
            this.bnContinuesMode.Name = "bnContinuesMode";
            this.bnContinuesMode.Size = new System.Drawing.Size(103, 28);
            this.bnContinuesMode.TabIndex = 0;
            this.bnContinuesMode.TabStop = true;
            this.bnContinuesMode.Text = "连续模式";
            this.bnContinuesMode.UseVisualStyleBackColor = true;
            this.bnContinuesMode.CheckedChanged += new System.EventHandler(this.bnContinuesMode_CheckedChanged);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox13);
            this.tabPage7.Controls.Add(this.groupBox6);
            this.tabPage7.Controls.Add(this.Demarcate_Btn);
            this.tabPage7.Controls.Add(this.groupBox2);
            this.tabPage7.Controls.Add(this.groupBox3);
            this.tabPage7.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage7.Location = new System.Drawing.Point(4, 4);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(379, 681);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "标定";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.Point4_Txt);
            this.groupBox13.Controls.Add(this.Point3_Txt);
            this.groupBox13.Controls.Add(this.Point2_Txt);
            this.groupBox13.Controls.Add(this.label22);
            this.groupBox13.Controls.Add(this.label21);
            this.groupBox13.Controls.Add(this.label13);
            this.groupBox13.Controls.Add(this.Point1_Txt);
            this.groupBox13.Controls.Add(this.label12);
            this.groupBox13.Location = new System.Drawing.Point(27, 503);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(326, 172);
            this.groupBox13.TabIndex = 50;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "4点面积";
            // 
            // Point4_Txt
            // 
            this.Point4_Txt.Location = new System.Drawing.Point(112, 137);
            this.Point4_Txt.Margin = new System.Windows.Forms.Padding(4);
            this.Point4_Txt.Name = "Point4_Txt";
            this.Point4_Txt.Size = new System.Drawing.Size(148, 30);
            this.Point4_Txt.TabIndex = 50;
            this.Point4_Txt.Text = "0";
            // 
            // Point3_Txt
            // 
            this.Point3_Txt.Location = new System.Drawing.Point(112, 102);
            this.Point3_Txt.Margin = new System.Windows.Forms.Padding(4);
            this.Point3_Txt.Name = "Point3_Txt";
            this.Point3_Txt.Size = new System.Drawing.Size(148, 30);
            this.Point3_Txt.TabIndex = 50;
            this.Point3_Txt.Text = "0";
            // 
            // Point2_Txt
            // 
            this.Point2_Txt.Location = new System.Drawing.Point(112, 64);
            this.Point2_Txt.Margin = new System.Windows.Forms.Padding(4);
            this.Point2_Txt.Name = "Point2_Txt";
            this.Point2_Txt.Size = new System.Drawing.Size(148, 30);
            this.Point2_Txt.TabIndex = 50;
            this.Point2_Txt.Text = "0";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label22.Location = new System.Drawing.Point(18, 137);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 30);
            this.label22.TabIndex = 49;
            this.label22.Text = "点4";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label21.Location = new System.Drawing.Point(18, 102);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 30);
            this.label21.TabIndex = 49;
            this.label21.Text = "点3";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(18, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 30);
            this.label13.TabIndex = 49;
            this.label13.Text = "点2";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Point1_Txt
            // 
            this.Point1_Txt.Location = new System.Drawing.Point(112, 26);
            this.Point1_Txt.Margin = new System.Windows.Forms.Padding(4);
            this.Point1_Txt.Name = "Point1_Txt";
            this.Point1_Txt.Size = new System.Drawing.Size(148, 30);
            this.Point1_Txt.TabIndex = 48;
            this.Point1_Txt.Text = "0";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.Location = new System.Drawing.Point(18, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 30);
            this.label12.TabIndex = 46;
            this.label12.Text = "点1";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.TinValue_Btn);
            this.groupBox6.Controls.Add(this.TinValue);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Location = new System.Drawing.Point(27, 413);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(326, 84);
            this.groupBox6.TabIndex = 43;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "参数设置";
            // 
            // TinValue_Btn
            // 
            this.TinValue_Btn.Location = new System.Drawing.Point(245, 35);
            this.TinValue_Btn.Name = "TinValue_Btn";
            this.TinValue_Btn.Size = new System.Drawing.Size(64, 35);
            this.TinValue_Btn.TabIndex = 49;
            this.TinValue_Btn.Text = "保存";
            this.TinValue_Btn.UseVisualStyleBackColor = true;
            this.TinValue_Btn.Click += new System.EventHandler(this.TinValue_Btn_Click);
            // 
            // TinValue
            // 
            this.TinValue.Location = new System.Drawing.Point(121, 37);
            this.TinValue.Margin = new System.Windows.Forms.Padding(4);
            this.TinValue.Name = "TinValue";
            this.TinValue.Size = new System.Drawing.Size(103, 30);
            this.TinValue.TabIndex = 48;
            this.TinValue.Text = "0";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(18, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 30);
            this.label9.TabIndex = 46;
            this.label9.Text = "检测值";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Demarcate_Btn
            // 
            this.Demarcate_Btn.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Demarcate_Btn.Location = new System.Drawing.Point(27, 371);
            this.Demarcate_Btn.Name = "Demarcate_Btn";
            this.Demarcate_Btn.Size = new System.Drawing.Size(326, 36);
            this.Demarcate_Btn.TabIndex = 42;
            this.Demarcate_Btn.Text = "标定";
            this.Demarcate_Btn.UseVisualStyleBackColor = true;
            this.Demarcate_Btn.Click += new System.EventHandler(this.Demarcate_Btn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.Qy_Row1);
            this.groupBox2.Controls.Add(this.Qx_Column1);
            this.groupBox2.Controls.Add(this.Qy_Row4);
            this.groupBox2.Controls.Add(this.Qx_Column4);
            this.groupBox2.Controls.Add(this.Qy_Row3);
            this.groupBox2.Controls.Add(this.Qx_Column3);
            this.groupBox2.Controls.Add(this.Qy_Row2);
            this.groupBox2.Controls.Add(this.Qx_Column2);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(27, 193);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(326, 172);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "激光坐标";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(18, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 28);
            this.label5.TabIndex = 47;
            this.label5.Text = "点4";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(18, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 28);
            this.label6.TabIndex = 46;
            this.label6.Text = "点3";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(18, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 28);
            this.label7.TabIndex = 45;
            this.label7.Text = "点2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(18, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 28);
            this.label8.TabIndex = 44;
            this.label8.Text = "点1";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Qy_Row1
            // 
            this.Qy_Row1.DecimalPlaces = 2;
            this.Qy_Row1.Location = new System.Drawing.Point(195, 27);
            this.Qy_Row1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qy_Row1.Name = "Qy_Row1";
            this.Qy_Row1.Size = new System.Drawing.Size(114, 30);
            this.Qy_Row1.TabIndex = 38;
            // 
            // Qx_Column1
            // 
            this.Qx_Column1.DecimalPlaces = 2;
            this.Qx_Column1.Location = new System.Drawing.Point(75, 26);
            this.Qx_Column1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qx_Column1.Name = "Qx_Column1";
            this.Qx_Column1.Size = new System.Drawing.Size(114, 30);
            this.Qx_Column1.TabIndex = 37;
            // 
            // Qy_Row4
            // 
            this.Qy_Row4.DecimalPlaces = 2;
            this.Qy_Row4.Location = new System.Drawing.Point(195, 134);
            this.Qy_Row4.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qy_Row4.Name = "Qy_Row4";
            this.Qy_Row4.Size = new System.Drawing.Size(114, 30);
            this.Qy_Row4.TabIndex = 36;
            // 
            // Qx_Column4
            // 
            this.Qx_Column4.DecimalPlaces = 2;
            this.Qx_Column4.Location = new System.Drawing.Point(75, 134);
            this.Qx_Column4.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qx_Column4.Name = "Qx_Column4";
            this.Qx_Column4.Size = new System.Drawing.Size(114, 30);
            this.Qx_Column4.TabIndex = 35;
            // 
            // Qy_Row3
            // 
            this.Qy_Row3.DecimalPlaces = 2;
            this.Qy_Row3.Location = new System.Drawing.Point(195, 98);
            this.Qy_Row3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qy_Row3.Name = "Qy_Row3";
            this.Qy_Row3.Size = new System.Drawing.Size(114, 30);
            this.Qy_Row3.TabIndex = 33;
            // 
            // Qx_Column3
            // 
            this.Qx_Column3.DecimalPlaces = 2;
            this.Qx_Column3.Location = new System.Drawing.Point(75, 98);
            this.Qx_Column3.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qx_Column3.Name = "Qx_Column3";
            this.Qx_Column3.Size = new System.Drawing.Size(114, 30);
            this.Qx_Column3.TabIndex = 32;
            // 
            // Qy_Row2
            // 
            this.Qy_Row2.DecimalPlaces = 2;
            this.Qy_Row2.Location = new System.Drawing.Point(195, 60);
            this.Qy_Row2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qy_Row2.Name = "Qy_Row2";
            this.Qy_Row2.Size = new System.Drawing.Size(114, 30);
            this.Qy_Row2.TabIndex = 30;
            // 
            // Qx_Column2
            // 
            this.Qx_Column2.DecimalPlaces = 2;
            this.Qx_Column2.Location = new System.Drawing.Point(75, 62);
            this.Qx_Column2.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.Qx_Column2.Name = "Qx_Column2";
            this.Qx_Column2.Size = new System.Drawing.Size(114, 30);
            this.Qx_Column2.TabIndex = 29;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.Py_Row4);
            this.groupBox3.Controls.Add(this.Px_Column4);
            this.groupBox3.Controls.Add(this.Py_Row3);
            this.groupBox3.Controls.Add(this.Px_Column3);
            this.groupBox3.Controls.Add(this.Py_Row2);
            this.groupBox3.Controls.Add(this.Px_Column1);
            this.groupBox3.Controls.Add(this.Px_Column2);
            this.groupBox3.Controls.Add(this.Py_Row1);
            this.groupBox3.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(27, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox3.Size = new System.Drawing.Size(326, 175);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "图像坐标";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(18, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 28);
            this.label4.TabIndex = 43;
            this.label4.Text = "点4";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(18, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 28);
            this.label3.TabIndex = 42;
            this.label3.Text = "点3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(18, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 28);
            this.label2.TabIndex = 41;
            this.label2.Text = "点2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(18, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 28);
            this.label1.TabIndex = 40;
            this.label1.Text = "点1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Py_Row4
            // 
            this.Py_Row4.DecimalPlaces = 2;
            this.Py_Row4.Location = new System.Drawing.Point(195, 131);
            this.Py_Row4.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Py_Row4.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Py_Row4.Name = "Py_Row4";
            this.Py_Row4.Size = new System.Drawing.Size(114, 30);
            this.Py_Row4.TabIndex = 36;
            // 
            // Px_Column4
            // 
            this.Px_Column4.DecimalPlaces = 2;
            this.Px_Column4.Location = new System.Drawing.Point(75, 131);
            this.Px_Column4.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Px_Column4.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Px_Column4.Name = "Px_Column4";
            this.Px_Column4.Size = new System.Drawing.Size(114, 30);
            this.Px_Column4.TabIndex = 35;
            // 
            // Py_Row3
            // 
            this.Py_Row3.DecimalPlaces = 2;
            this.Py_Row3.Location = new System.Drawing.Point(195, 93);
            this.Py_Row3.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Py_Row3.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Py_Row3.Name = "Py_Row3";
            this.Py_Row3.Size = new System.Drawing.Size(114, 30);
            this.Py_Row3.TabIndex = 33;
            // 
            // Px_Column3
            // 
            this.Px_Column3.DecimalPlaces = 2;
            this.Px_Column3.Location = new System.Drawing.Point(75, 95);
            this.Px_Column3.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Px_Column3.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Px_Column3.Name = "Px_Column3";
            this.Px_Column3.Size = new System.Drawing.Size(114, 30);
            this.Px_Column3.TabIndex = 32;
            // 
            // Py_Row2
            // 
            this.Py_Row2.DecimalPlaces = 2;
            this.Py_Row2.Location = new System.Drawing.Point(195, 59);
            this.Py_Row2.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Py_Row2.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Py_Row2.Name = "Py_Row2";
            this.Py_Row2.Size = new System.Drawing.Size(114, 30);
            this.Py_Row2.TabIndex = 30;
            // 
            // Px_Column1
            // 
            this.Px_Column1.DecimalPlaces = 2;
            this.Px_Column1.Location = new System.Drawing.Point(75, 23);
            this.Px_Column1.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Px_Column1.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Px_Column1.Name = "Px_Column1";
            this.Px_Column1.Size = new System.Drawing.Size(114, 30);
            this.Px_Column1.TabIndex = 29;
            // 
            // Px_Column2
            // 
            this.Px_Column2.DecimalPlaces = 2;
            this.Px_Column2.Location = new System.Drawing.Point(75, 59);
            this.Px_Column2.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Px_Column2.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Px_Column2.Name = "Px_Column2";
            this.Px_Column2.Size = new System.Drawing.Size(114, 30);
            this.Px_Column2.TabIndex = 29;
            // 
            // Py_Row1
            // 
            this.Py_Row1.DecimalPlaces = 2;
            this.Py_Row1.Location = new System.Drawing.Point(195, 23);
            this.Py_Row1.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Py_Row1.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Py_Row1.Name = "Py_Row1";
            this.Py_Row1.Size = new System.Drawing.Size(114, 30);
            this.Py_Row1.TabIndex = 27;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox12);
            this.tabPage8.Controls.Add(this.groupBox11);
            this.tabPage8.Controls.Add(this.Model_Save_Btn);
            this.tabPage8.Controls.Add(this.button14);
            this.tabPage8.Controls.Add(this.button13);
            this.tabPage8.Controls.Add(this.CreateMode_Button);
            this.tabPage8.Controls.Add(this.Product_Comb);
            this.tabPage8.Controls.Add(this.button17);
            this.tabPage8.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage8.Location = new System.Drawing.Point(4, 4);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(379, 681);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "模型";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.SaveOffset_Btn);
            this.groupBox12.Controls.Add(this.label20);
            this.groupBox12.Controls.Add(this.label18);
            this.groupBox12.Controls.Add(this.label16);
            this.groupBox12.Controls.Add(this.label44);
            this.groupBox12.Controls.Add(this.label19);
            this.groupBox12.Controls.Add(this.label17);
            this.groupBox12.Controls.Add(this.label10);
            this.groupBox12.Controls.Add(this.label43);
            this.groupBox12.Controls.Add(this.Y4offset_Nud);
            this.groupBox12.Controls.Add(this.Y3offset_Nud);
            this.groupBox12.Controls.Add(this.Y2offset_Nud);
            this.groupBox12.Controls.Add(this.Y1offset_Nud);
            this.groupBox12.Controls.Add(this.X4offset_Nud);
            this.groupBox12.Controls.Add(this.X3offset_Nud);
            this.groupBox12.Controls.Add(this.X2offset_Nud);
            this.groupBox12.Controls.Add(this.X1offset_Nud);
            this.groupBox12.Location = new System.Drawing.Point(18, 49);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(351, 221);
            this.groupBox12.TabIndex = 52;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "偏移补偿";
            // 
            // SaveOffset_Btn
            // 
            this.SaveOffset_Btn.Location = new System.Drawing.Point(252, 174);
            this.SaveOffset_Btn.Name = "SaveOffset_Btn";
            this.SaveOffset_Btn.Size = new System.Drawing.Size(65, 38);
            this.SaveOffset_Btn.TabIndex = 53;
            this.SaveOffset_Btn.Text = "保存";
            this.SaveOffset_Btn.UseVisualStyleBackColor = true;
            this.SaveOffset_Btn.Click += new System.EventHandler(this.SaveOffset_Btn_Click);
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.Location = new System.Drawing.Point(173, 139);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 28);
            this.label20.TabIndex = 42;
            this.label20.Text = "点4y:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.Location = new System.Drawing.Point(173, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 28);
            this.label18.TabIndex = 42;
            this.label18.Text = "点3y:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(173, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 28);
            this.label16.TabIndex = 42;
            this.label16.Text = "点2y:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label44.Location = new System.Drawing.Point(173, 31);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 28);
            this.label44.TabIndex = 42;
            this.label44.Text = "点1y:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.Location = new System.Drawing.Point(6, 139);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 28);
            this.label19.TabIndex = 41;
            this.label19.Text = "点4x:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(6, 103);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 28);
            this.label17.TabIndex = 41;
            this.label17.Text = "点3x:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(6, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 28);
            this.label10.TabIndex = 41;
            this.label10.Text = "点2x:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label43.Location = new System.Drawing.Point(6, 31);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(59, 28);
            this.label43.TabIndex = 41;
            this.label43.Text = "点1x:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Y4offset_Nud
            // 
            this.Y4offset_Nud.DecimalPlaces = 2;
            this.Y4offset_Nud.Location = new System.Drawing.Point(238, 138);
            this.Y4offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Y4offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Y4offset_Nud.Name = "Y4offset_Nud";
            this.Y4offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.Y4offset_Nud.TabIndex = 39;
            // 
            // Y3offset_Nud
            // 
            this.Y3offset_Nud.DecimalPlaces = 2;
            this.Y3offset_Nud.Location = new System.Drawing.Point(238, 102);
            this.Y3offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Y3offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Y3offset_Nud.Name = "Y3offset_Nud";
            this.Y3offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.Y3offset_Nud.TabIndex = 39;
            // 
            // Y2offset_Nud
            // 
            this.Y2offset_Nud.DecimalPlaces = 2;
            this.Y2offset_Nud.Location = new System.Drawing.Point(238, 66);
            this.Y2offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Y2offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Y2offset_Nud.Name = "Y2offset_Nud";
            this.Y2offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.Y2offset_Nud.TabIndex = 39;
            // 
            // Y1offset_Nud
            // 
            this.Y1offset_Nud.DecimalPlaces = 2;
            this.Y1offset_Nud.Location = new System.Drawing.Point(238, 30);
            this.Y1offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.Y1offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.Y1offset_Nud.Name = "Y1offset_Nud";
            this.Y1offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.Y1offset_Nud.TabIndex = 39;
            // 
            // X4offset_Nud
            // 
            this.X4offset_Nud.DecimalPlaces = 2;
            this.X4offset_Nud.Location = new System.Drawing.Point(75, 138);
            this.X4offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.X4offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.X4offset_Nud.Name = "X4offset_Nud";
            this.X4offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.X4offset_Nud.TabIndex = 38;
            // 
            // X3offset_Nud
            // 
            this.X3offset_Nud.DecimalPlaces = 2;
            this.X3offset_Nud.Location = new System.Drawing.Point(75, 102);
            this.X3offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.X3offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.X3offset_Nud.Name = "X3offset_Nud";
            this.X3offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.X3offset_Nud.TabIndex = 38;
            // 
            // X2offset_Nud
            // 
            this.X2offset_Nud.DecimalPlaces = 2;
            this.X2offset_Nud.Location = new System.Drawing.Point(75, 66);
            this.X2offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.X2offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.X2offset_Nud.Name = "X2offset_Nud";
            this.X2offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.X2offset_Nud.TabIndex = 38;
            // 
            // X1offset_Nud
            // 
            this.X1offset_Nud.DecimalPlaces = 2;
            this.X1offset_Nud.Location = new System.Drawing.Point(75, 30);
            this.X1offset_Nud.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.X1offset_Nud.Minimum = new decimal(new int[] {
            20000,
            0,
            0,
            -2147483648});
            this.X1offset_Nud.Name = "X1offset_Nud";
            this.X1offset_Nud.Size = new System.Drawing.Size(77, 30);
            this.X1offset_Nud.TabIndex = 38;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.roi_Column1);
            this.groupBox11.Controls.Add(this.Roi_Save_Btn);
            this.groupBox11.Controls.Add(this.roi_Row1);
            this.groupBox11.Controls.Add(this.roi_Column2);
            this.groupBox11.Controls.Add(this.roi_Row2);
            this.groupBox11.Controls.Add(this.label37);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Location = new System.Drawing.Point(18, 276);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(351, 143);
            this.groupBox11.TabIndex = 47;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "ROI值";
            // 
            // roi_Column1
            // 
            this.roi_Column1.DecimalPlaces = 2;
            this.roi_Column1.Location = new System.Drawing.Point(229, 31);
            this.roi_Column1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.roi_Column1.Name = "roi_Column1";
            this.roi_Column1.Size = new System.Drawing.Size(105, 30);
            this.roi_Column1.TabIndex = 59;
            // 
            // Roi_Save_Btn
            // 
            this.Roi_Save_Btn.Location = new System.Drawing.Point(28, 99);
            this.Roi_Save_Btn.Name = "Roi_Save_Btn";
            this.Roi_Save_Btn.Size = new System.Drawing.Size(73, 38);
            this.Roi_Save_Btn.TabIndex = 58;
            this.Roi_Save_Btn.Text = "保存";
            this.Roi_Save_Btn.UseVisualStyleBackColor = true;
            this.Roi_Save_Btn.Click += new System.EventHandler(this.Roi_Save_Btn_Click);
            // 
            // roi_Row1
            // 
            this.roi_Row1.DecimalPlaces = 2;
            this.roi_Row1.Location = new System.Drawing.Point(118, 31);
            this.roi_Row1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.roi_Row1.Name = "roi_Row1";
            this.roi_Row1.Size = new System.Drawing.Size(105, 30);
            this.roi_Row1.TabIndex = 56;
            // 
            // roi_Column2
            // 
            this.roi_Column2.DecimalPlaces = 2;
            this.roi_Column2.Location = new System.Drawing.Point(229, 65);
            this.roi_Column2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.roi_Column2.Name = "roi_Column2";
            this.roi_Column2.Size = new System.Drawing.Size(105, 30);
            this.roi_Column2.TabIndex = 54;
            // 
            // roi_Row2
            // 
            this.roi_Row2.DecimalPlaces = 2;
            this.roi_Row2.Location = new System.Drawing.Point(118, 65);
            this.roi_Row2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.roi_Row2.Name = "roi_Row2";
            this.roi_Row2.Size = new System.Drawing.Size(105, 30);
            this.roi_Row2.TabIndex = 53;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label37.Location = new System.Drawing.Point(30, 65);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 30);
            this.label37.TabIndex = 51;
            this.label37.Text = "点2";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(30, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 30);
            this.label11.TabIndex = 51;
            this.label11.Text = "点1";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Model_Save_Btn
            // 
            this.Model_Save_Btn.Location = new System.Drawing.Point(270, 637);
            this.Model_Save_Btn.Name = "Model_Save_Btn";
            this.Model_Save_Btn.Size = new System.Drawing.Size(78, 38);
            this.Model_Save_Btn.TabIndex = 46;
            this.Model_Save_Btn.Text = "保存";
            this.Model_Save_Btn.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(186, 637);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(78, 38);
            this.button14.TabIndex = 45;
            this.button14.Text = "加载";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(102, 637);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(78, 38);
            this.button13.TabIndex = 44;
            this.button13.Text = "删除";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // CreateMode_Button
            // 
            this.CreateMode_Button.Location = new System.Drawing.Point(18, 637);
            this.CreateMode_Button.Name = "CreateMode_Button";
            this.CreateMode_Button.Size = new System.Drawing.Size(78, 38);
            this.CreateMode_Button.TabIndex = 6;
            this.CreateMode_Button.Text = "创建";
            this.CreateMode_Button.UseVisualStyleBackColor = true;
            this.CreateMode_Button.Click += new System.EventHandler(this.CreateMode_Button_Click);
            // 
            // Product_Comb
            // 
            this.Product_Comb.FormattingEnabled = true;
            this.Product_Comb.Items.AddRange(new object[] {
            "model1",
            "model2"});
            this.Product_Comb.Location = new System.Drawing.Point(180, 12);
            this.Product_Comb.Name = "Product_Comb";
            this.Product_Comb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Product_Comb.Size = new System.Drawing.Size(108, 31);
            this.Product_Comb.TabIndex = 40;
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button17.Location = new System.Drawing.Point(18, 12);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(108, 31);
            this.button17.TabIndex = 39;
            this.button17.Text = "产品模型";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.label48);
            this.tabPage10.Controls.Add(this.rtbMySendMessage);
            this.tabPage10.Controls.Add(this.rtbReceiveMsg);
            this.tabPage10.Controls.Add(this.pictureBox2);
            this.tabPage10.Controls.Add(this.btnConnect);
            this.tabPage10.Controls.Add(this.label47);
            this.tabPage10.Controls.Add(this.label46);
            this.tabPage10.Controls.Add(this.txbServerPort);
            this.tabPage10.Controls.Add(this.txbServerIP);
            this.tabPage10.Controls.Add(this.label42);
            this.tabPage10.Controls.Add(this.label41);
            this.tabPage10.Controls.Add(this.textBox4);
            this.tabPage10.Controls.Add(this.textBox3);
            this.tabPage10.Controls.Add(this.textBox2);
            this.tabPage10.Controls.Add(this.textBox1);
            this.tabPage10.Controls.Add(this.label40);
            this.tabPage10.Controls.Add(this.label39);
            this.tabPage10.Controls.Add(this.label38);
            this.tabPage10.Controls.Add(this.button2);
            this.tabPage10.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage10.Location = new System.Drawing.Point(4, 4);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(379, 681);
            this.tabPage10.TabIndex = 4;
            this.tabPage10.Text = "通讯";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(23, 154);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(97, 32);
            this.label48.TabIndex = 25;
            this.label48.Text = "接受数据";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rtbMySendMessage
            // 
            this.rtbMySendMessage.Location = new System.Drawing.Point(142, 201);
            this.rtbMySendMessage.Multiline = true;
            this.rtbMySendMessage.Name = "rtbMySendMessage";
            this.rtbMySendMessage.Size = new System.Drawing.Size(138, 30);
            this.rtbMySendMessage.TabIndex = 24;
            // 
            // rtbReceiveMsg
            // 
            this.rtbReceiveMsg.Location = new System.Drawing.Point(142, 155);
            this.rtbReceiveMsg.Multiline = true;
            this.rtbReceiveMsg.Name = "rtbReceiveMsg";
            this.rtbReceiveMsg.Size = new System.Drawing.Size(138, 30);
            this.rtbReceiveMsg.TabIndex = 24;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::VisionBasicSystem.Properties.Resources.red;
            this.pictureBox2.Location = new System.Drawing.Point(142, 103);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 35);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(23, 100);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(101, 40);
            this.btnConnect.TabIndex = 22;
            this.btnConnect.Text = "启动监听";
            this.btnConnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(23, 58);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(97, 27);
            this.label47.TabIndex = 21;
            this.label47.Text = "端口号";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(23, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(97, 27);
            this.label46.TabIndex = 21;
            this.label46.Text = "服务器IP";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txbServerPort
            // 
            this.txbServerPort.Location = new System.Drawing.Point(142, 56);
            this.txbServerPort.Name = "txbServerPort";
            this.txbServerPort.Size = new System.Drawing.Size(138, 30);
            this.txbServerPort.TabIndex = 20;
            this.txbServerPort.Text = "5000";
            // 
            // txbServerIP
            // 
            this.txbServerIP.Location = new System.Drawing.Point(142, 20);
            this.txbServerIP.Name = "txbServerIP";
            this.txbServerIP.Size = new System.Drawing.Size(138, 30);
            this.txbServerIP.TabIndex = 20;
            this.txbServerIP.Text = "192.168.0.10";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label42.Location = new System.Drawing.Point(23, 355);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(97, 32);
            this.label42.TabIndex = 16;
            this.label42.Text = "结束文本";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label41.Location = new System.Drawing.Point(23, 318);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(97, 32);
            this.label41.TabIndex = 15;
            this.label41.Text = "行结束符";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(142, 356);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(138, 30);
            this.textBox4.TabIndex = 14;
            this.textBox4.Text = "End\\r\\n";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(142, 319);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(138, 30);
            this.textBox3.TabIndex = 13;
            this.textBox3.Text = ".";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(142, 284);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(138, 30);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = ";";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(142, 255);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(138, 30);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "inspect";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label40.Location = new System.Drawing.Point(23, 283);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(97, 32);
            this.label40.TabIndex = 10;
            this.label40.Text = "分隔符";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("微软雅黑", 10.28571F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label39.Location = new System.Drawing.Point(27, 254);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(97, 32);
            this.label39.TabIndex = 9;
            this.label39.Text = "开头文本";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(43, 246);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(8, 8);
            this.label38.TabIndex = 8;
            this.label38.Text = "label38";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(23, 196);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(101, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "发送数据";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnMySendMessage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(13, 58);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(885, 682);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Date_Lab
            // 
            this.Date_Lab.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Date_Lab.Location = new System.Drawing.Point(1064, 15);
            this.Date_Lab.Name = "Date_Lab";
            this.Date_Lab.Size = new System.Drawing.Size(214, 29);
            this.Date_Lab.TabIndex = 7;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label23.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label23.Location = new System.Drawing.Point(905, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(153, 29);
            this.label23.TabIndex = 8;
            this.label23.Text = "欢迎登录系统!";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 6000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1330, 753);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.Date_Lab);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbDeviceList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "云智动视觉系统";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Basic_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qy_Row2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Qx_Column2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Px_Column2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Py_Row1)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Y4offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y3offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y2offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Y1offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X4offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X3offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X2offset_Nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.X1offset_Nud)).EndInit();
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.roi_Column1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Row1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Column2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roi_Row2)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bnClose;
        private System.Windows.Forms.Button bnEnum;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbDeviceList;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button bnSetParam;
        private System.Windows.Forms.Button bnGetParam;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbFrameRate;
        private System.Windows.Forms.TextBox tbGain;
        private System.Windows.Forms.TextBox tbExposure;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button bnSaveJpg;
        private System.Windows.Forms.Button bnSaveBmp;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button bnTriggerExec;
        private System.Windows.Forms.CheckBox cbSoftTrigger;
        private System.Windows.Forms.Button bnStopGrab;
        private System.Windows.Forms.Button bnStartGrab;
        private System.Windows.Forms.RadioButton bnTriggerMode;
        private System.Windows.Forms.RadioButton bnContinuesMode;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button Demarcate_Btn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown Qy_Row1;
        private System.Windows.Forms.NumericUpDown Qx_Column1;
        private System.Windows.Forms.NumericUpDown Qy_Row4;
        private System.Windows.Forms.NumericUpDown Qx_Column4;
        private System.Windows.Forms.NumericUpDown Qy_Row3;
        private System.Windows.Forms.NumericUpDown Qx_Column3;
        private System.Windows.Forms.NumericUpDown Qy_Row2;
        private System.Windows.Forms.NumericUpDown Qx_Column2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown Py_Row4;
        private System.Windows.Forms.NumericUpDown Px_Column4;
        private System.Windows.Forms.NumericUpDown Py_Row3;
        private System.Windows.Forms.NumericUpDown Px_Column3;
        private System.Windows.Forms.NumericUpDown Py_Row2;
        private System.Windows.Forms.NumericUpDown Px_Column2;
        private System.Windows.Forms.NumericUpDown Py_Row1;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button Model_Save_Btn;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button CreateMode_Button;
        private System.Windows.Forms.ComboBox Product_Comb;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.NumericUpDown Px_Column1;
        private System.Windows.Forms.Button bnOpen;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown roi_Column2;
        private System.Windows.Forms.NumericUpDown roi_Row2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txbServerIP;
        private System.Windows.Forms.TextBox txbServerPort;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox rtbReceiveMsg;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox rtbMySendMessage;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox TinValue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button TinValue_Btn;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox Point4_Txt;
        private System.Windows.Forms.TextBox Point3_Txt;
        private System.Windows.Forms.TextBox Point2_Txt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Point1_Txt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Date_Lab;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.NumericUpDown roi_Row1;
        private System.Windows.Forms.Button Roi_Save_Btn;
        private System.Windows.Forms.NumericUpDown roi_Column1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button SaveOffset_Btn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.NumericUpDown Y4offset_Nud;
        private System.Windows.Forms.NumericUpDown Y3offset_Nud;
        private System.Windows.Forms.NumericUpDown Y2offset_Nud;
        private System.Windows.Forms.NumericUpDown Y1offset_Nud;
        private System.Windows.Forms.NumericUpDown X4offset_Nud;
        private System.Windows.Forms.NumericUpDown X3offset_Nud;
        private System.Windows.Forms.NumericUpDown X2offset_Nud;
        private System.Windows.Forms.NumericUpDown X1offset_Nud;
        //private HalconDotNet.HWindowControl hWindowControl2;
    }
}

