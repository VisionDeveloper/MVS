﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem
{
    //相机相关的常量类
    internal class CameraConstants
    {
        //加载图片
        public const string LOAD_IMAGE = "\\image1.bmp";

        //日志级别
        public const string LOGS_INFO = "Info";

        public const string LOGS_ERROR = "Error";
    }
}