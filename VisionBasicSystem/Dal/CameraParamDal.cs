﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using VisionBasicSystem.Commons;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Dal
{
    public class CameraParamDal
    {
        //设置相机参数
        public int SetCameraParam(CameraParam cameraParam)
        {
            string sql = "UPDATE camera_param SET exposure=@Exposure, gain=@Gain,frame_rate=@FrameRate WHERE id=1";
            MySqlParameter[] pars = {
                new MySqlParameter("@Exposure", MySqlDbType.Int32),
                new MySqlParameter("@Gain", MySqlDbType.Int32),
                new MySqlParameter("@FrameRate", MySqlDbType.Int32)
            };
            pars[0].Value = cameraParam.Exposure;
            pars[1].Value = cameraParam.Gain;
            pars[2].Value = cameraParam.FrameRate;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //根据编号查询相机参数
        public CameraParam GetCameraParam()
        {
            string sql = "SELECT * FROM camera_param WHERE id = 1";
            DataTable da = MySqlHelperUtil.GetDataTable(sql, CommandType.Text);
            CameraParam cameraParam = null;
            if (da.Rows.Count > 0)
            {
                cameraParam = new CameraParam();
                LoadEntity(cameraParam, da.Rows[0]);
            }
            return cameraParam;
        }

        //将row中的数据赋值给CameraParam的属性
        private void LoadEntity(CameraParam cameraParam, DataRow row)
        {
            cameraParam.Id = Convert.ToInt32(row["id"]);
            cameraParam.Exposure = Convert.ToInt32(row["exposure"]);
            cameraParam.Gain = Convert.ToDecimal(row["gain"]);
            cameraParam.FrameRate = Convert.ToInt32(row["frame_rate"]);
        }
    }
}