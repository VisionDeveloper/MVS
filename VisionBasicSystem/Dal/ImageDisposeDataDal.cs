﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using VisionBasicSystem.Commons;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Dal
{
    public class ImageDisposeDataDal
    {
        //根据ID获取图片处理数据
        public ImageDisposeData GetImageData()
        {
            string sql = "SELECT * FROM image_dispose_data WHERE id = 1";
            DataTable da = MySqlHelperUtil.GetDataTable(sql, CommandType.Text);
            ImageDisposeData imageDispose = null;
            if (da.Rows.Count > 0)
            {
                imageDispose = new ImageDisposeData();
                LoadEntity(imageDispose, da.Rows[0]);
            }
            return imageDispose;
        }

        //修改偏移值
        public int EditOffset(ImageDisposeData imageDispose)
        {
            string sql = "UPDATE image_dispose_data SET x1offset=@X1offset,y1offset=@Y1offset,x2offset=@X2offset,y2offset=@Y2offset,x3offset=@X3offset,y3offset=@Y3offset,x4offset=@X4offset,y4offset=@Y4offset WHERE id = 1";

            MySqlParameter[] pars = {
                new MySqlParameter("@X1offset",MySqlDbType.Decimal),
                new MySqlParameter("@Y1offset",MySqlDbType.Decimal),
                new MySqlParameter("@X2offset",MySqlDbType.Decimal),
                new MySqlParameter("@Y2offset",MySqlDbType.Decimal),
                new MySqlParameter("@X3offset",MySqlDbType.Decimal),
                new MySqlParameter("@Y3offset",MySqlDbType.Decimal),
                new MySqlParameter("@X4offset",MySqlDbType.Decimal),
                new MySqlParameter("@Y4offset",MySqlDbType.Decimal)
            };
            pars[0].Value = imageDispose.X1offset;
            pars[1].Value = imageDispose.Y1offset;
            pars[2].Value = imageDispose.X2offset;
            pars[3].Value = imageDispose.Y2offset;
            pars[4].Value = imageDispose.X3offset;
            pars[5].Value = imageDispose.Y3offset;
            pars[6].Value = imageDispose.X4offset;
            pars[7].Value = imageDispose.Y4offset;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //修改锡点面积值
        public int EditTinValue(ImageDisposeData imageDispose)
        {
            string sql = "UPDATE image_dispose_data SET tin_value=@TinValue WHERE id = 1";

            MySqlParameter[] pars = {
                new MySqlParameter("@TinValue", MySqlDbType.Int32)
            };
            pars[0].Value = imageDispose.TinValue;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //更新标定坐标
        public int UpdateCalibrationValue(ImageDisposeData imageDispose)
        {
            string sql = "UPDATE image_dispose_data SET laser1x=@Laser1x, laser1y=@Laser1y,laser2x=@Laser2x,laser2y=@Laser2y," +
                "laser3x=@Laser3x,laser3y=@Laser3y,laser4x=@Laser4x,laser4y=@Laser4y WHERE id = 1";

            MySqlParameter[] pars = {
                new MySqlParameter("@Laser1x", MySqlDbType.Decimal),
                new MySqlParameter("@Laser1y", MySqlDbType.Decimal),
                new MySqlParameter("@Laser2x", MySqlDbType.Decimal),
                new MySqlParameter("@Laser2y", MySqlDbType.Decimal),
                new MySqlParameter("@Laser3x", MySqlDbType.Decimal),
                new MySqlParameter("@Laser3y", MySqlDbType.Decimal),
                new MySqlParameter("@Laser4x", MySqlDbType.Decimal),
                new MySqlParameter("@Laser4y", MySqlDbType.Decimal)
            };
            pars[0].Value = imageDispose.Laser1x;
            pars[1].Value = imageDispose.Laser1y;
            pars[2].Value = imageDispose.Laser2x;
            pars[3].Value = imageDispose.Laser2y;
            pars[4].Value = imageDispose.Laser3x;
            pars[5].Value = imageDispose.Laser3y;
            pars[6].Value = imageDispose.Laser4x;
            pars[7].Value = imageDispose.Laser4y;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //将row中的数据赋值给userinfo的属性
        private void LoadEntity(ImageDisposeData imageDispose, DataRow row)
        {
            imageDispose.Id = Convert.ToInt32(row["id"]);
            imageDispose.X1offset = Convert.ToDecimal(row["x1offset"]);
            imageDispose.Y1offset = Convert.ToDecimal(row["y1offset"]);
            imageDispose.X2offset = Convert.ToDecimal(row["x2offset"]);
            imageDispose.Y2offset = Convert.ToDecimal(row["y2offset"]);
            imageDispose.X3offset = Convert.ToDecimal(row["x3offset"]);
            imageDispose.Y3offset = Convert.ToDecimal(row["y3offset"]);
            imageDispose.X4offset = Convert.ToDecimal(row["x4offset"]);
            imageDispose.Y4offset = Convert.ToDecimal(row["y4offset"]);
            imageDispose.TinValue = Convert.ToInt32(row["tin_value"]);
            imageDispose.Laser1x = Convert.ToDecimal(row["laser1x"]);
            imageDispose.Laser1y = Convert.ToDecimal(row["laser1y"]);
            imageDispose.Laser2x = Convert.ToDecimal(row["laser2x"]);
            imageDispose.Laser2y = Convert.ToDecimal(row["laser2y"]);
            imageDispose.Laser3x = Convert.ToDecimal(row["laser3x"]);
            imageDispose.Laser3y = Convert.ToDecimal(row["laser3y"]);
            imageDispose.Laser4x = Convert.ToDecimal(row["laser4x"]);
            imageDispose.Laser4y = Convert.ToDecimal(row["laser4y"]);
        }
    }
}