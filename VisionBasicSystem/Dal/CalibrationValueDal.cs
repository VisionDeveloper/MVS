﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionBasicSystem.Commons;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Dal
{
    public class CalibrationValueDal
    {
        //根据编号查询标定值
        public CalibrationValue GetCalibrationValue()
        {
            string sql = "SELECT * FROM calibration_value WHERE id = 1";
            DataTable da = MySqlHelperUtil.GetDataTable(sql, CommandType.Text);
            CalibrationValue calibrationValue = null;
            if (da.Rows.Count > 0)
            {
                calibrationValue = new CalibrationValue();
                LoadEntity(calibrationValue, da.Rows[0]);
            }
            return calibrationValue;
        }

        //更新标定值
        public int UpdateCalibrationValue(CalibrationValue calibrationValue)
        {
            string sql = "UPDATE calibration_value SET value=@Value WHERE id=1";
            MySqlParameter[] pars = {
              new MySqlParameter("@Value",MySqlDbType.VarChar)
            };
            pars[0].Value = calibrationValue.Value;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //将row中的数据赋值给CalibrationValue的属性
        private void LoadEntity(CalibrationValue calibrationValue, DataRow row)
        {
            calibrationValue.Id = Convert.ToInt32(row["id"]);
            calibrationValue.Value = row["value"] != DBNull.Value ? row["value"].ToString() : string.Empty;
        }
    }
}