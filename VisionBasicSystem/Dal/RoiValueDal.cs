﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisionBasicSystem.Commons;
using VisionBasicSystem.Model;

namespace VisionBasicSystem.Dal
{
    //ROI值
    public class RoiValueDal
    {
        //设置ROI值
        public int UpdateRoiValue(RoiValue roiValue)
        {
            string sql = "UPDATE roi_value SET row1=@Row1,column1=@Column1, row2=@Row2,column2=@Column2 WHERE id=1";
            MySqlParameter[] pars = {
                new MySqlParameter("@Row1", MySqlDbType.Decimal),
                new MySqlParameter("@Column1", MySqlDbType.Decimal),
                new MySqlParameter("@Row2", MySqlDbType.Decimal),
                new MySqlParameter("@Column2", MySqlDbType.Decimal),
            };
            pars[0].Value = roiValue.Row1;
            pars[1].Value = roiValue.Column1;
            pars[2].Value = roiValue.Row2;
            pars[3].Value = roiValue.Column2;
            return MySqlHelperUtil.ExecuteNonquery(sql, CommandType.Text, pars);
        }

        //根据编号查询ROI值
        public RoiValue GetRoiValue()
        {
            string sql = "SELECT * FROM roi_value WHERE id = 1";
            DataTable da = MySqlHelperUtil.GetDataTable(sql, CommandType.Text);
            RoiValue roiValue = null;
            if (da.Rows.Count > 0)
            {
                roiValue = new RoiValue();
                LoadEntity(roiValue, da.Rows[0]);
            }
            return roiValue;
        }

        //将row中的数据赋值给RoiValue的属性
        private void LoadEntity(RoiValue roiValue, DataRow row)
        {
            roiValue.Id = Convert.ToInt32(row["id"]);
            roiValue.Row1 = Convert.ToDecimal(row["row1"]);
            roiValue.Column1 = Convert.ToDecimal(row["column1"]);
            roiValue.Row2 = Convert.ToDecimal(row["row2"]);
            roiValue.Column2 = Convert.ToDecimal(row["column2"]);
        }
    }
}