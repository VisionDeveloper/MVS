using System;
using System.Text;
using System.Collections;

namespace VisionBasicSystem
{
    internal class ByteUtils
    {
        //字符串拆分
        public static ArrayList Str16ToArrayList(string strIn)
        {
            string sParse = "";
            ArrayList myAL = new ArrayList();
            int i = 0;
            foreach (char cc in strIn)
            {
                i++;
                if (cc == ' ' || sParse.Length == 2)
                {
                    myAL.Add(sParse);
                    if (sParse.Length == 2 && cc != ' ')//两个字符
                    {
                        sParse = Convert.ToString(cc);
                    }
                    else
                    {
                        sParse = "";
                    }
                }
                else
                {
                    sParse += Convert.ToString(cc);
                    if (i == strIn.Length && cc != ' ')//末尾字符
                    {
                        myAL.Add(sParse);
                    }
                }
            }
            return myAL;
        }

        //以两个或空格问区分字符串增加空格
        public static string StrAddSpace(string strInData)
        {
            string strParse = "";
            string strAddResult = "";
            int i = 0;
            foreach (char cc in strInData)
            {
                i++;
                if (cc == ' ' || strParse.Length == 2)
                {
                    strAddResult += strParse + " ";
                    //两个字符
                    if (strParse.Length == 2 && cc != ' ')
                    {
                        strParse = Convert.ToString(cc);
                    }
                    else
                    {
                        strParse = "";
                    }
                }
                else
                {
                    strParse += Convert.ToString(cc);
                }
                //末尾字符
                if (i == strInData.Length && cc != ' ')
                {
                    strAddResult += strParse;
                }
            }

            return strAddResult;
        }

        //去除字符串中空格并把单个字符串前面加0
        public static string StrSubSpace(string strInData)
        {
            //将头和尾组合成一样的形式
            string strParse = " " + strInData + " ";
            string strParseTemp = "";
            string strSubResult = "";

            //int i = 0;
            for (int i = 0; i < strParse.Length; i++)
            {
                //char myChar
                /*if ((i == 0) && (strParse[1]==' '))
                {
                    strParseTemp = "0" + strParse[0].ToString();
                    strSubResult += strParseTemp;
                }
                else*/
                if ((i > 0) && (i < strInData.Length + 1))
                {
                    if ((strParse[i] != ' ') && (strParse[i - 1] == ' ') && (strParse[i + 1] == ' '))
                    {
                        strParseTemp = "0" + strParse[i].ToString();
                        strSubResult += strParseTemp;
                    }
                    else
                    {
                        strParseTemp = strParse[i].ToString();
                        strSubResult += strParseTemp;
                    }
                }
            }
            strSubResult = strSubResult.Replace(" ", "");
            return strSubResult;
        }

        /// <summary>
        /// 十六进制数组形式的CRC—A001
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] CRC_16_A001(byte[] data)
        {
            //int len = data.Length;
            //ushort tt;
            if (data.Length > 0)
            {
                ushort crc = 0xFFFF;
                for (int i = 0; i < data.Length; i++)
                {
                    crc = (ushort)(crc ^ (data[i]));
                    for (int j = 0; j < 8; j++)
                    {
                        #region
                        //也可以用功能相同
                        /*tt = (ushort)(crc & 1);
                        crc = (ushort)(crc >> 1);
                        crc = (ushort)(crc & 0x7fff);
                        if (tt == 1)
                        {
                            crc = (ushort)(crc ^ 0xa001);
                        }
                        crc = (ushort)(crc & 0xffff);*/
                        #endregion
                        crc = (crc & 1) != 0 ? (ushort)((crc >> 1) ^ 0xA001) : (ushort)(crc >> 1);
                    }
                }
                byte hi = (byte)((crc & 0xFFFF) >> 8);//高位置
                byte lo = (byte)(crc & 0xFFFF);//低位置
                return new byte[] { hi, lo };
            }
            return new byte[] { 0, 0 };
        }

        public static byte CS(byte[] dataIn)//十六进制数组形式的CS
        {
            byte resultVal = 0;
            for (int i = 0; i < dataIn.Length; i++)
            {
                resultVal += dataIn[i];
            }
            return resultVal;
        }

        //ASCII字符串转换为Hex字节
        public static byte[] StrAsciiToHexBy(string inStr)
        {
            //string s = "";
            ArrayList al = Str16ToArrayList(inStr);
            byte[] by = new byte[al.Count];
            int i = 0;
            foreach (string stmp in al)
            {
                //将指定基的数字的字符串表示形式转换为等效的 8 位无符号整数。
                by[i] += Convert.ToByte(stmp, 16);
                i++;
            }
            //s = Encoding.GetEncoding("Gb2312").GetString(by);//在派生类中重写时，将指定字节数组中的所有字节解码为一个字符串。。
            return by;//s;
        }

        //ASCII字符串转换为Hex字符串
        public static string StrAsciiToHexStr(string inStr)
        {
            string s = "";
            ArrayList al = Str16ToArrayList(inStr);
            byte[] by = new byte[al.Count];
            int i = 0;
            foreach (string stmp in al)
            {
                //将指定基的数字的字符串表示形式转换为等效的 8 位无符号整数。
                by[i] += Convert.ToByte(stmp, 16);
                i++;
            }
            //在派生类中重写时，将指定字节数组中的所有字节解码为一个字符串。。
            s = Encoding.GetEncoding("Gb2312").GetString(by);
            return s;
        }

        public static byte ByLength(byte[] by)//总长度
        {
            byte myBy;
            myBy = Convert.ToByte(by.Length);
            return myBy;
        }

        //总长度
        public static Int16 int16Length(byte[] by)
        {
            Int16 myBy;
            myBy = Convert.ToByte(by.Length);
            return myBy;
        }

        /// 将一条十六进制字符串转换为ASCII
        /// </summary>
        /// <param name="hexstring">一条十六进制字符串</param>
        /// <returns>返回一条ASCII码</returns>
        public static string HexStringToASCII(string hexstring)
        {
            byte[] bt = HexStringToBinary(hexstring);
            string lin = "";
            for (int i = 0; i < bt.Length; i++)
            {
                lin = lin + bt[i] + " ";
            }

            string[] ss = lin.Trim().Split(new char[] { ' ' });
            char[] c = new char[ss.Length];
            int a;
            for (int i = 0; i < c.Length; i++)
            {
                a = Convert.ToInt32(ss[i]);
                c[i] = Convert.ToChar(a);
            }

            string b = new string(c);
            return b;
        }

        /// <summary>
        /// 16进制字符串转换为二进制数组
        /// </summary>
        /// <param name="hexstring">字符串每个字节之间都应该有空格，大多数的串口通讯资料上面的16进制都是字节之间都是用空格来分割的。</param>
        /// <returns>返回一个二进制字符串</returns>
        public static byte[] HexStringToBinary(string hexstring)
        {
            string[] tmpary = hexstring.Trim().Split(' ');
            byte[] buff = new byte[tmpary.Length];
            for (int i = 0; i < buff.Length; i++)
            {
                buff[i] = Convert.ToByte(tmpary[i], 16);
            }
            return buff;
        }
    }
}