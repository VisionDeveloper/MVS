﻿using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VisionBasicSystem.Commons
{
    internal class LogHelper
    {
        //这里的 loginfo 和 log4net.config 里的名字要一样
        public static readonly ILog loginfo = LogManager.GetLogger("loginfo");

        //这里的 logerror 和 log4net.config 里的名字要一样
        public static readonly ILog logerror = LogManager.GetLogger("logerror");

        //记录正常信息
        public static void WriteLog(string info)
        {
            if (loginfo.IsInfoEnabled)
            {
                loginfo.Info(info);
            }
        }

        //记录异常信息
        public static void WriteLog(string info, Exception ex)
        {
            if (logerror.IsErrorEnabled)
            {
                StackTrace stackTrace = new StackTrace();
                StackFrame stackFrame = stackTrace.GetFrame(1);
                MethodBase methodBase = stackFrame.GetMethod();
                logerror.Error("类名:" + methodBase.ReflectedType.Name + " 方法名:" + methodBase.Name + " 信息:" + info, ex);
            }
        }
    }
}