﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace VisionBasicSystem
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            //实现启动程序单例
            bool canCreateNew;
            Mutex m = new Mutex(true, "视觉识别系统", out canCreateNew);
            if (canCreateNew)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
                m.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("程序已经运行！");
            }
        }
    }
}